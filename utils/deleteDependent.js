const db = require('mongoose');
let Sourchrecomendation = require('../model/sourchrecomendation')(db);
let Recomendation = require('../model/recomendation')(db);
let Trend_record = require('../model/trend_record')(db);
let Fatmor_record = require('../model/fatmor_record')(db);
let Wish_record = require('../model/wish_record')(db);
let Kios_sehat_record = require('../model/kios_sehat_record')(db);
let User = require('../model/user')(db);
let UserTokens = require('../model/userTokens')(db);
let Role = require('../model/role')(db);
let ProjectRoute = require('../model/projectRoute')(db);
let RouteRole = require('../model/routeRole')(db);
let UserRole = require('../model/userRole')(db);

const deleteSourchrecomendation = async (filter) =>{
  try {
    let sourchrecomendation = await Sourchrecomendation.find(filter, { _id:1 });
    if (sourchrecomendation.length){
      sourchrecomendation = sourchrecomendation.map((obj) => obj._id);
      const recomendationFilter8353 = { 'sourch': { '$in': sourchrecomendation } };
      const recomendation4787 = await deleteRecomendation(recomendationFilter8353);
      return await Sourchrecomendation.deleteMany(filter);
    } else {
      return 'No sourchrecomendation found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteRecomendation = async (filter) =>{
  try {
    return await Recomendation.deleteMany(filter);
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteTrend_record = async (filter) =>{
  try {
    return await Trend_record.deleteMany(filter);
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteFatmor_record = async (filter) =>{
  try {
    return await Fatmor_record.deleteMany(filter);
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteWish_record = async (filter) =>{
  try {
    return await Wish_record.deleteMany(filter);
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteKios_sehat_record = async (filter) =>{
  try {
    return await Kios_sehat_record.deleteMany(filter);
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteUser = async (filter) =>{
  try {
    let user = await User.find(filter, { _id:1 });
    if (user.length){
      user = user.map((obj) => obj._id);
      const trend_recordFilter0492 = { 'user': { '$in': user } };
      const trend_record7401 = await deleteTrend_record(trend_recordFilter0492);
      const fatmor_recordFilter8463 = { 'user': { '$in': user } };
      const fatmor_record5667 = await deleteFatmor_record(fatmor_recordFilter8463);
      const wish_recordFilter5405 = { 'user': { '$in': user } };
      const wish_record2368 = await deleteWish_record(wish_recordFilter5405);
      const kios_sehat_recordFilter3157 = { 'user': { '$in': user } };
      const kios_sehat_record8680 = await deleteKios_sehat_record(kios_sehat_recordFilter3157);
      const userFilter2015 = { 'addedBy': { '$in': user } };
      const user3078 = await deleteUser(userFilter2015);
      const userFilter3496 = { 'updatedBy': { '$in': user } };
      const user7293 = await deleteUser(userFilter3496);
      const userTokensFilter9132 = { 'userId': { '$in': user } };
      const userTokens5161 = await deleteUserTokens(userTokensFilter9132);
      const userRoleFilter4380 = { 'userId': { '$in': user } };
      const userRole3366 = await deleteUserRole(userRoleFilter4380);
      return await User.deleteMany(filter);
    } else {
      return 'No user found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteUserTokens = async (filter) =>{
  try {
    return await UserTokens.deleteMany(filter);
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteRole = async (filter) =>{
  try {
    let role = await Role.find(filter, { _id:1 });
    if (role.length){
      role = role.map((obj) => obj._id);
      const routeRoleFilter7902 = { 'roleId': { '$in': role } };
      const routeRole4659 = await deleteRouteRole(routeRoleFilter7902);
      const userRoleFilter7925 = { 'roleId': { '$in': role } };
      const userRole7664 = await deleteUserRole(userRoleFilter7925);
      return await Role.deleteMany(filter);
    } else {
      return 'No role found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteProjectRoute = async (filter) =>{
  try {
    let projectroute = await ProjectRoute.find(filter, { _id:1 });
    if (projectroute.length){
      projectroute = projectroute.map((obj) => obj._id);
      const routeRoleFilter7933 = { 'routeId': { '$in': projectroute } };
      const routeRole6596 = await deleteRouteRole(routeRoleFilter7933);
      return await ProjectRoute.deleteMany(filter);
    } else {
      return 'No projectRoute found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteRouteRole = async (filter) =>{
  try {
    return await RouteRole.deleteMany(filter);
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteUserRole = async (filter) =>{
  try {
    return await UserRole.deleteMany(filter);
  } catch (error){
    throw new Error(error.message);
  }
};

const countSourchrecomendation = async (filter) =>{
  try {
    let sourchrecomendation = await Sourchrecomendation.find(filter, { _id:1 });
    if (sourchrecomendation.length){
      sourchrecomendation = sourchrecomendation.map((obj) => obj._id);

      const recomendationFilter = { '$or': [{                    sourch : { '$in' : sourchrecomendation } }] };
      const recomendationCnt =  await Recomendation.countDocuments(recomendationFilter);
           
      let response = { recomendation : recomendationCnt, };
            
      return response;
    } else {
      return {  sourchrecomendation : 0 };
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const countRecomendation = async (filter) =>{
  try {
    const recomendationCnt =  await Recomendation.countDocuments(filter);
    return { recomendation : recomendationCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countTrend_record = async (filter) =>{
  try {
    const trend_recordCnt =  await Trend_record.countDocuments(filter);
    return { trend_record : trend_recordCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countFatmor_record = async (filter) =>{
  try {
    const fatmor_recordCnt =  await Fatmor_record.countDocuments(filter);
    return { fatmor_record : fatmor_recordCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countWish_record = async (filter) =>{
  try {
    const wish_recordCnt =  await Wish_record.countDocuments(filter);
    return { wish_record : wish_recordCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countKios_sehat_record = async (filter) =>{
  try {
    const kios_sehat_recordCnt =  await Kios_sehat_record.countDocuments(filter);
    return { kios_sehat_record : kios_sehat_recordCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countUser = async (filter) =>{
  try {
    let user = await User.find(filter, { _id:1 });
    if (user.length){
      user = user.map((obj) => obj._id);

      const trend_recordFilter = { '$or': [{                    user : { '$in' : user } }] };
      const trend_recordCnt =  await Trend_record.countDocuments(trend_recordFilter);

      const fatmor_recordFilter = { '$or': [{                    user : { '$in' : user } }] };
      const fatmor_recordCnt =  await Fatmor_record.countDocuments(fatmor_recordFilter);

      const wish_recordFilter = { '$or': [{                    user : { '$in' : user } }] };
      const wish_recordCnt =  await Wish_record.countDocuments(wish_recordFilter);

      const kios_sehat_recordFilter = { '$or': [{                    user : { '$in' : user } }] };
      const kios_sehat_recordCnt =  await Kios_sehat_record.countDocuments(kios_sehat_recordFilter);

      const userFilter = { '$or': [{                    addedBy : { '$in' : user } },{                    updatedBy : { '$in' : user } }] };
      const userCnt =  await User.countDocuments(userFilter);

      const userTokensFilter = { '$or': [{                    userId : { '$in' : user } }] };
      const userTokensCnt =  await UserTokens.countDocuments(userTokensFilter);

      const userRoleFilter = { '$or': [{                    userId : { '$in' : user } }] };
      const userRoleCnt =  await UserRole.countDocuments(userRoleFilter);
           
      let response = {
        trend_record : trend_recordCnt,
        fatmor_record : fatmor_recordCnt,
        wish_record : wish_recordCnt,
        kios_sehat_record : kios_sehat_recordCnt,
        user : userCnt,
        userTokens : userTokensCnt,
        userRole : userRoleCnt,
      };
            
      return response;
    } else {
      return {  user : 0 };
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const countUserTokens = async (filter) =>{
  try {
    const userTokensCnt =  await UserTokens.countDocuments(filter);
    return { userTokens : userTokensCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countRole = async (filter) =>{
  try {
    let role = await Role.find(filter, { _id:1 });
    if (role.length){
      role = role.map((obj) => obj._id);

      const routeRoleFilter = { '$or': [{                    roleId : { '$in' : role } }] };
      const routeRoleCnt =  await RouteRole.countDocuments(routeRoleFilter);

      const userRoleFilter = { '$or': [{                    roleId : { '$in' : role } }] };
      const userRoleCnt =  await UserRole.countDocuments(userRoleFilter);
           
      let response = {
        routeRole : routeRoleCnt,
        userRole : userRoleCnt,
      };
            
      return response;
    } else {
      return {  role : 0 };
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const countProjectRoute = async (filter) =>{
  try {
    let projectroute = await ProjectRoute.find(filter, { _id:1 });
    if (projectroute.length){
      projectroute = projectroute.map((obj) => obj._id);

      const routeRoleFilter = { '$or': [{                    routeId : { '$in' : projectroute } }] };
      const routeRoleCnt =  await RouteRole.countDocuments(routeRoleFilter);
           
      let response = { routeRole : routeRoleCnt, };
            
      return response;
    } else {
      return {  projectroute : 0 };
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const countRouteRole = async (filter) =>{
  try {
    const routeRoleCnt =  await RouteRole.countDocuments(filter);
    return { routeRole : routeRoleCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countUserRole = async (filter) =>{
  try {
    const userRoleCnt =  await UserRole.countDocuments(filter);
    return { userRole : userRoleCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteSourchrecomendation = async (filter,updateBody, defaultValues = {}) =>{
  try {
    let sourchrecomendation = await Sourchrecomendation.find(filter, { _id:1 });
    if (sourchrecomendation.length){
      sourchrecomendation = sourchrecomendation.map((obj) => obj._id);
      const recomendationFilter6956 = { 'sourch': { '$in': sourchrecomendation } };
      const recomendation3596 = await softDeleteRecomendation(recomendationFilter6956, updateBody);
      return await Sourchrecomendation.updateMany(filter, {
        ...defaultValues,
        ...updateBody
      });
    } else {
      return 'No sourchrecomendation found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteRecomendation = async (filter,updateBody, defaultValues = {}) =>{
  try {
    return await Recomendation.updateMany(filter, {
      ...defaultValues,
      ...updateBody
    });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteTrend_record = async (filter,updateBody, defaultValues = {}) =>{
  try {
    return await Trend_record.updateMany(filter, {
      ...defaultValues,
      ...updateBody
    });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteFatmor_record = async (filter,updateBody, defaultValues = {}) =>{
  try {
    return await Fatmor_record.updateMany(filter, {
      ...defaultValues,
      ...updateBody
    });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteWish_record = async (filter,updateBody, defaultValues = {}) =>{
  try {
    return await Wish_record.updateMany(filter, {
      ...defaultValues,
      ...updateBody
    });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteKios_sehat_record = async (filter,updateBody, defaultValues = {}) =>{
  try {
    return await Kios_sehat_record.updateMany(filter, {
      ...defaultValues,
      ...updateBody
    });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteUser = async (filter,updateBody, defaultValues = {}) =>{
  try {
    let user = await User.find(filter, { _id:1 });
    if (user.length){
      user = user.map((obj) => obj._id);
      const trend_recordFilter3384 = { 'user': { '$in': user } };
      const trend_record2407 = await softDeleteTrend_record(trend_recordFilter3384, updateBody);
      const fatmor_recordFilter6121 = { 'user': { '$in': user } };
      const fatmor_record3473 = await softDeleteFatmor_record(fatmor_recordFilter6121, updateBody);
      const wish_recordFilter0455 = { 'user': { '$in': user } };
      const wish_record4622 = await softDeleteWish_record(wish_recordFilter0455, updateBody);
      const kios_sehat_recordFilter4585 = { 'user': { '$in': user } };
      const kios_sehat_record3646 = await softDeleteKios_sehat_record(kios_sehat_recordFilter4585, updateBody);
      const userFilter7414 = { 'addedBy': { '$in': user } };
      const user8194 = await softDeleteUser(userFilter7414, updateBody);
      const userFilter6925 = { 'updatedBy': { '$in': user } };
      const user8130 = await softDeleteUser(userFilter6925, updateBody);
      const userTokensFilter1583 = { 'userId': { '$in': user } };
      const userTokens5670 = await softDeleteUserTokens(userTokensFilter1583, updateBody);
      const userRoleFilter6636 = { 'userId': { '$in': user } };
      const userRole5089 = await softDeleteUserRole(userRoleFilter6636, updateBody);
      return await User.updateMany(filter, {
        ...defaultValues,
        ...updateBody
      });
    } else {
      return 'No user found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteUserTokens = async (filter,updateBody, defaultValues = {}) =>{
  try {
    return await UserTokens.updateMany(filter, {
      ...defaultValues,
      ...updateBody
    });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteRole = async (filter,updateBody, defaultValues = {}) =>{
  try {
    let role = await Role.find(filter, { _id:1 });
    if (role.length){
      role = role.map((obj) => obj._id);
      const routeRoleFilter6722 = { 'roleId': { '$in': role } };
      const routeRole9330 = await softDeleteRouteRole(routeRoleFilter6722, updateBody);
      const userRoleFilter6566 = { 'roleId': { '$in': role } };
      const userRole8043 = await softDeleteUserRole(userRoleFilter6566, updateBody);
      return await Role.updateMany(filter, {
        ...defaultValues,
        ...updateBody
      });
    } else {
      return 'No role found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteProjectRoute = async (filter,updateBody, defaultValues = {}) =>{
  try {
    let projectroute = await ProjectRoute.find(filter, { _id:1 });
    if (projectroute.length){
      projectroute = projectroute.map((obj) => obj._id);
      const routeRoleFilter7782 = { 'routeId': { '$in': projectroute } };
      const routeRole5878 = await softDeleteRouteRole(routeRoleFilter7782, updateBody);
      return await ProjectRoute.updateMany(filter, {
        ...defaultValues,
        ...updateBody
      });
    } else {
      return 'No projectRoute found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteRouteRole = async (filter,updateBody, defaultValues = {}) =>{
  try {
    return await RouteRole.updateMany(filter, {
      ...defaultValues,
      ...updateBody
    });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteUserRole = async (filter,updateBody, defaultValues = {}) =>{
  try {
    return await UserRole.updateMany(filter, {
      ...defaultValues,
      ...updateBody
    });
  } catch (error){
    throw new Error(error.message);
  }
};

module.exports = {
  deleteSourchrecomendation,
  deleteRecomendation,
  deleteTrend_record,
  deleteFatmor_record,
  deleteWish_record,
  deleteKios_sehat_record,
  deleteUser,
  deleteUserTokens,
  deleteRole,
  deleteProjectRoute,
  deleteRouteRole,
  deleteUserRole,
  countSourchrecomendation,
  countRecomendation,
  countTrend_record,
  countFatmor_record,
  countWish_record,
  countKios_sehat_record,
  countUser,
  countUserTokens,
  countRole,
  countProjectRoute,
  countRouteRole,
  countUserRole,
  softDeleteSourchrecomendation,
  softDeleteRecomendation,
  softDeleteTrend_record,
  softDeleteFatmor_record,
  softDeleteWish_record,
  softDeleteKios_sehat_record,
  softDeleteUser,
  softDeleteUserTokens,
  softDeleteRole,
  softDeleteProjectRoute,
  softDeleteRouteRole,
  softDeleteUserRole,
};
