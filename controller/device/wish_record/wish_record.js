const message = require('../../../utils/messages');

function makeWish_recordController ({
  wish_recordService,makeWish_record
})
{
  const addWish_record = async ({
    data, loggedInUser
  }) => {
    try {
      let originalData = data;
      originalData.addedBy = loggedInUser.id.toString();
      const wish_record = makeWish_record(originalData,'insertWish_recordValidator');
      let createdWish_record = await wish_recordService.createDocument(wish_record);
      return message.successResponse(
        { data :  createdWish_record }
      );
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const findAllWish_record = async ({
    data, loggedInUser
  }) => {
    try {
      let options = {};
      let query = {};
      let result;
      if (data.query !== undefined) {
        query = { ...data.query };
      }
      if (data.isCountOnly){
        result = await wish_recordService.countDocument(query);
        if (result) {
          result = { totalRecords: result };  
          return message.successResponse(result);
        } else {
          return message.recordNotFound();
        }
      } else { 
        if (data.options !== undefined) {
          options = { ...data.options };
        }
        result = await wish_recordService.getAllDocuments(query,options);
      }
      if (result.data){
        return message.successResponse({ data: result });
      } else {
        return message.recordNotFound();
      }
            
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getWish_recordCount = async (data) => {
    try {
      let where = {};
      if (data && data.where){
        where = data.where;
      }
      let result = await wish_recordService.countDocument(where);
      result = { totalRecords:result };
      return message.successResponse({ data: result });
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getWish_recordByAggregate = async ({ data }) =>{
    try {
      if (data){
        let result = await wish_recordService.getDocumentByAggregation(data);
        if (result && result.length){
          return message.successResponse({ data: result });
        }
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse(); 
    }
  };

  const softDeleteManyWish_record = async (ids, loggedInUser) => {
    try {
      if (ids){
        const query = { _id:{ $in:ids } };
        const updateBody = { isDeleted: true, };
        let data = await wish_recordService.bulkUpdate(query, updateBody);
        return message.successResponse({ data:data });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkInsertWish_record = async ({
    body, loggedInUser
  }) => {
    try {
      let data = body.data;
      for (let i = 0;i < data.length;i++){
        data[i] = {
          ...data[i],
          addedBy:loggedInUser.id.toString(),
        };
      }
      const wish_recordEntities = data.map((item)=>makeWish_record(item,'insertWish_recordValidator'));
      const results = await wish_recordService.bulkInsert(wish_recordEntities);
      return message.successResponse({ data:results });
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkUpdateWish_record = async (data, loggedInUser) => {
    try {
      if (data.filter && data.data){
        delete data.data['addedBy'];
        delete data.data['updatedBy'];
        data.data.updatedBy = loggedInUser.id;
        const wish_record = makeWish_record(data.data,'updateWish_recordValidator');
        const filterData = removeEmpty(wish_record);
        let query = data.filter;
        const updatedWish_records = await wish_recordService.bulkUpdate(query,filterData);
        return message.successResponse({ data:updatedWish_records });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteManyWish_record = async (data, loggedInUser) => {
    try {
      if (data && data.ids){
        let ids = data.ids;
        const query = { '_id':{ '$in':ids } };
        let result = await wish_recordService.deleteMany(query);
        return message.successResponse({ data:result });
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const softDeleteWish_record = async (id,loggedInUser)=>{
    try {
      if (id){
        const query = { _id:id };
        const updateBody = { isDeleted: true, };
        let updatedWish_record = await wish_recordService.softDeleteByQuery(query, updateBody);
        return message.successResponse({ data:updatedWish_record });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message: error.message });
      }
      return message.failureResponse();
    }
  };

  const partialUpdateWish_record = async (data,id, loggedInUser) => {
    try {
      if (id && data){
        delete data['addedBy'];
        delete data['updatedBy'];
        wish_record.updatedBy = loggedInUser.id;
        const wish_record = makeWish_record(data,'updateWish_recordValidator');            
        const filterData = removeEmpty(wish_record);
        const query = { _id:id };
        let updatedWish_record = await wish_recordService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedWish_record){
          return message.successResponse({ data : updatedWish_record });
        }
        else {
          return message.badRequest();
        }
      }
      else {
        return message.badRequest();
      }
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const updateWish_record = async (data,id, loggedInUser) =>{
    try {
      delete data['addedBy'];
      delete data['updatedBy'];
      data.updatedBy = loggedInUser.id;
      if (id && data){
        const wish_record = makeWish_record(data,'updateWish_recordValidator');
        const filterData = removeEmpty(wish_record);
        let query = { _id:id };
        let updatedWish_record = await wish_recordService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedWish_record){
          return message.successResponse({ data : updatedWish_record });
        }
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const getWish_recordById = async (query, body = {}) =>{
    try {
      if (query){
        let options = {};
        if (body && body.populate && body.populate.length) options.populate = body.populate;
        if (body && body.select && body.select.length) options.select = body.select;
        let result = await wish_recordService.getSingleDocument(query, options);
        if (result){
          return message.successResponse({ data: result });
        }
        return message.recordNotFound();
                 
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteWish_record = async (data,id,loggedInUser) => {
    try {
      if (id){
        const query = { _id:id };
        let deletedWish_record = await wish_recordService.findOneAndDeleteDocument(query);
        return message.successResponse({ data: deletedWish_record });
                
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const removeEmpty = (obj) => {
    Object.entries(obj).forEach(([key,value])=>{
      if (value === undefined){
        delete obj[key];
      }
    });
    return obj;
  };

  return Object.freeze({
    addWish_record,
    findAllWish_record,
    getWish_recordCount,
    getWish_recordByAggregate,
    softDeleteManyWish_record,
    bulkInsertWish_record,
    bulkUpdateWish_record,
    deleteManyWish_record,
    softDeleteWish_record,
    partialUpdateWish_record,
    updateWish_record,
    getWish_recordById,
    deleteWish_record,
    removeEmpty,
  });
}

module.exports = makeWish_recordController;
