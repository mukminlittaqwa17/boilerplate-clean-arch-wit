const db = require('mongoose');
const sourchrecomendationModel = require('../../../model/sourchrecomendation')(db);
const {
  schemaKeys,updateSchemaKeys
} = require('../../../validation/sourchrecomendationValidation');
const insertSourchrecomendationValidator = require('../../../validation/genericValidator')(schemaKeys);
const updateSourchrecomendationValidator = require('../../../validation/genericValidator')(updateSchemaKeys);
const makeSourchrecomendation = require('../../../entity/sourchrecomendation')({
  insertSourchrecomendationValidator,
  updateSourchrecomendationValidator
});
const sourchrecomendationService = require('../../../services/mongoDbService')({
  model:sourchrecomendationModel,
  makeSourchrecomendation
});
const makeSourchrecomendationController = require('./sourchrecomendation');
const sourchrecomendationController = makeSourchrecomendationController({
  sourchrecomendationService,
  makeSourchrecomendation
});
module.exports = sourchrecomendationController;
