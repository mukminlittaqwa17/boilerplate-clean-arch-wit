const db = require('mongoose');
const fatmor_recordModel = require('../../../model/fatmor_record')(db);
const {
  schemaKeys,updateSchemaKeys
} = require('../../../validation/fatmor_recordValidation');
const insertFatmor_recordValidator = require('../../../validation/genericValidator')(schemaKeys);
const updateFatmor_recordValidator = require('../../../validation/genericValidator')(updateSchemaKeys);
const makeFatmor_record = require('../../../entity/fatmor_record')({
  insertFatmor_recordValidator,
  updateFatmor_recordValidator
});
const fatmor_recordService = require('../../../services/mongoDbService')({
  model:fatmor_recordModel,
  makeFatmor_record
});
const makeFatmor_recordController = require('./fatmor_record');
const fatmor_recordController = makeFatmor_recordController({
  fatmor_recordService,
  makeFatmor_record
});
module.exports = fatmor_recordController;
