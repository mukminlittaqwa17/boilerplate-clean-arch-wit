const message = require('../../../utils/messages');

function makeFatmor_recordController ({
  fatmor_recordService,makeFatmor_record
})
{
  const addFatmor_record = async ({
    data, loggedInUser
  }) => {
    try {
      let originalData = data;
      originalData.addedBy = loggedInUser.id.toString();
      const fatmor_record = makeFatmor_record(originalData,'insertFatmor_recordValidator');
      let createdFatmor_record = await fatmor_recordService.createDocument(fatmor_record);
      return message.successResponse(
        { data :  createdFatmor_record }
      );
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const findAllFatmor_record = async ({
    data, loggedInUser
  }) => {
    try {
      let options = {};
      let query = {};
      let result;
      if (data.query !== undefined) {
        query = { ...data.query };
      }
      if (data.isCountOnly){
        result = await fatmor_recordService.countDocument(query);
        if (result) {
          result = { totalRecords: result };  
          return message.successResponse(result);
        } else {
          return message.recordNotFound();
        }
      } else { 
        if (data.options !== undefined) {
          options = { ...data.options };
        }
        result = await fatmor_recordService.getAllDocuments(query,options);
      }
      if (result.data){
        return message.successResponse({ data: result });
      } else {
        return message.recordNotFound();
      }
            
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getFatmor_recordCount = async (data) => {
    try {
      let where = {};
      if (data && data.where){
        where = data.where;
      }
      let result = await fatmor_recordService.countDocument(where);
      result = { totalRecords:result };
      return message.successResponse({ data: result });
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getFatmor_recordByAggregate = async ({ data }) =>{
    try {
      if (data){
        let result = await fatmor_recordService.getDocumentByAggregation(data);
        if (result && result.length){
          return message.successResponse({ data: result });
        }
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse(); 
    }
  };

  const softDeleteManyFatmor_record = async (ids, loggedInUser) => {
    try {
      if (ids){
        const query = { _id:{ $in:ids } };
        const updateBody = { isDeleted: true, };
        let data = await fatmor_recordService.bulkUpdate(query, updateBody);
        return message.successResponse({ data:data });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkInsertFatmor_record = async ({
    body, loggedInUser
  }) => {
    try {
      let data = body.data;
      for (let i = 0;i < data.length;i++){
        data[i] = {
          ...data[i],
          addedBy:loggedInUser.id.toString(),
        };
      }
      const fatmor_recordEntities = data.map((item)=>makeFatmor_record(item,'insertFatmor_recordValidator'));
      const results = await fatmor_recordService.bulkInsert(fatmor_recordEntities);
      return message.successResponse({ data:results });
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkUpdateFatmor_record = async (data, loggedInUser) => {
    try {
      if (data.filter && data.data){
        delete data.data['addedBy'];
        delete data.data['updatedBy'];
        data.data.updatedBy = loggedInUser.id;
        const fatmor_record = makeFatmor_record(data.data,'updateFatmor_recordValidator');
        const filterData = removeEmpty(fatmor_record);
        let query = data.filter;
        const updatedFatmor_records = await fatmor_recordService.bulkUpdate(query,filterData);
        return message.successResponse({ data:updatedFatmor_records });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteManyFatmor_record = async (data, loggedInUser) => {
    try {
      if (data && data.ids){
        let ids = data.ids;
        const query = { '_id':{ '$in':ids } };
        let result = await fatmor_recordService.deleteMany(query);
        return message.successResponse({ data:result });
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const softDeleteFatmor_record = async (id,loggedInUser)=>{
    try {
      if (id){
        const query = { _id:id };
        const updateBody = { isDeleted: true, };
        let updatedFatmor_record = await fatmor_recordService.softDeleteByQuery(query, updateBody);
        return message.successResponse({ data:updatedFatmor_record });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message: error.message });
      }
      return message.failureResponse();
    }
  };

  const partialUpdateFatmor_record = async (data,id, loggedInUser) => {
    try {
      if (id && data){
        delete data['addedBy'];
        delete data['updatedBy'];
        fatmor_record.updatedBy = loggedInUser.id;
        const fatmor_record = makeFatmor_record(data,'updateFatmor_recordValidator');            
        const filterData = removeEmpty(fatmor_record);
        const query = { _id:id };
        let updatedFatmor_record = await fatmor_recordService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedFatmor_record){
          return message.successResponse({ data : updatedFatmor_record });
        }
        else {
          return message.badRequest();
        }
      }
      else {
        return message.badRequest();
      }
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const updateFatmor_record = async (data,id, loggedInUser) =>{
    try {
      delete data['addedBy'];
      delete data['updatedBy'];
      data.updatedBy = loggedInUser.id;
      if (id && data){
        const fatmor_record = makeFatmor_record(data,'updateFatmor_recordValidator');
        const filterData = removeEmpty(fatmor_record);
        let query = { _id:id };
        let updatedFatmor_record = await fatmor_recordService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedFatmor_record){
          return message.successResponse({ data : updatedFatmor_record });
        }
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const getFatmor_recordById = async (query, body = {}) =>{
    try {
      if (query){
        let options = {};
        if (body && body.populate && body.populate.length) options.populate = body.populate;
        if (body && body.select && body.select.length) options.select = body.select;
        let result = await fatmor_recordService.getSingleDocument(query, options);
        if (result){
          return message.successResponse({ data: result });
        }
        return message.recordNotFound();
                 
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteFatmor_record = async (data,id,loggedInUser) => {
    try {
      if (id){
        const query = { _id:id };
        let deletedFatmor_record = await fatmor_recordService.findOneAndDeleteDocument(query);
        return message.successResponse({ data: deletedFatmor_record });
                
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const removeEmpty = (obj) => {
    Object.entries(obj).forEach(([key,value])=>{
      if (value === undefined){
        delete obj[key];
      }
    });
    return obj;
  };

  return Object.freeze({
    addFatmor_record,
    findAllFatmor_record,
    getFatmor_recordCount,
    getFatmor_recordByAggregate,
    softDeleteManyFatmor_record,
    bulkInsertFatmor_record,
    bulkUpdateFatmor_record,
    deleteManyFatmor_record,
    softDeleteFatmor_record,
    partialUpdateFatmor_record,
    updateFatmor_record,
    getFatmor_recordById,
    deleteFatmor_record,
    removeEmpty,
  });
}

module.exports = makeFatmor_recordController;
