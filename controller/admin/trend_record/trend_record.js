const message = require('../../../utils/messages');

function makeTrend_recordController ({
  trend_recordService,makeTrend_record
})
{
  const addTrend_record = async ({
    data, loggedInUser
  }) => {
    try {
      let originalData = data;
      originalData.addedBy = loggedInUser.id.toString();
      const trend_record = makeTrend_record(originalData,'insertTrend_recordValidator');
      let createdTrend_record = await trend_recordService.createDocument(trend_record);
      return message.successResponse(
        { data :  createdTrend_record }
      );
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const findAllTrend_record = async ({
    data, loggedInUser
  }) => {
    try {
      let options = {};
      let query = {};
      let result;
      if (data.query !== undefined) {
        query = { ...data.query };
      }
      if (data.isCountOnly){
        result = await trend_recordService.countDocument(query);
        if (result) {
          result = { totalRecords: result };  
          return message.successResponse(result);
        } else {
          return message.recordNotFound();
        }
      } else { 
        if (data.options !== undefined) {
          options = { ...data.options };
        }
        result = await trend_recordService.getAllDocuments(query,options);
      }
      if (result.data){
        return message.successResponse({ data: result });
      } else {
        return message.recordNotFound();
      }
            
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getTrend_recordCount = async (data) => {
    try {
      let where = {};
      if (data && data.where){
        where = data.where;
      }
      let result = await trend_recordService.countDocument(where);
      result = { totalRecords:result };
      return message.successResponse({ data: result });
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getTrend_recordByAggregate = async ({ data }) =>{
    try {
      if (data){
        let result = await trend_recordService.getDocumentByAggregation(data);
        if (result && result.length){
          return message.successResponse({ data: result });
        }
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse(); 
    }
  };

  const softDeleteManyTrend_record = async (ids, loggedInUser) => {
    try {
      if (ids){
        const query = { _id:{ $in:ids } };
        const updateBody = { isDeleted: true, };
        let data = await trend_recordService.bulkUpdate(query, updateBody);
        return message.successResponse({ data:data });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkInsertTrend_record = async ({
    body, loggedInUser
  }) => {
    try {
      let data = body.data;
      for (let i = 0;i < data.length;i++){
        data[i] = {
          ...data[i],
          addedBy:loggedInUser.id.toString(),
        };
      }
      const trend_recordEntities = data.map((item)=>makeTrend_record(item,'insertTrend_recordValidator'));
      const results = await trend_recordService.bulkInsert(trend_recordEntities);
      return message.successResponse({ data:results });
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkUpdateTrend_record = async (data, loggedInUser) => {
    try {
      if (data.filter && data.data){
        delete data.data['addedBy'];
        delete data.data['updatedBy'];
        data.data.updatedBy = loggedInUser.id;
        const trend_record = makeTrend_record(data.data,'updateTrend_recordValidator');
        const filterData = removeEmpty(trend_record);
        let query = data.filter;
        const updatedTrend_records = await trend_recordService.bulkUpdate(query,filterData);
        return message.successResponse({ data:updatedTrend_records });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteManyTrend_record = async (data, loggedInUser) => {
    try {
      if (data && data.ids){
        let ids = data.ids;
        const query = { '_id':{ '$in':ids } };
        let result = await trend_recordService.deleteMany(query);
        return message.successResponse({ data:result });
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const softDeleteTrend_record = async (id,loggedInUser)=>{
    try {
      if (id){
        const query = { _id:id };
        const updateBody = { isDeleted: true, };
        let updatedTrend_record = await trend_recordService.softDeleteByQuery(query, updateBody);
        return message.successResponse({ data:updatedTrend_record });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message: error.message });
      }
      return message.failureResponse();
    }
  };

  const partialUpdateTrend_record = async (data,id, loggedInUser) => {
    try {
      if (id && data){
        delete data['addedBy'];
        delete data['updatedBy'];
        trend_record.updatedBy = loggedInUser.id;
        const trend_record = makeTrend_record(data,'updateTrend_recordValidator');            
        const filterData = removeEmpty(trend_record);
        const query = { _id:id };
        let updatedTrend_record = await trend_recordService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedTrend_record){
          return message.successResponse({ data : updatedTrend_record });
        }
        else {
          return message.badRequest();
        }
      }
      else {
        return message.badRequest();
      }
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const updateTrend_record = async (data,id, loggedInUser) =>{
    try {
      delete data['addedBy'];
      delete data['updatedBy'];
      data.updatedBy = loggedInUser.id;
      if (id && data){
        const trend_record = makeTrend_record(data,'updateTrend_recordValidator');
        const filterData = removeEmpty(trend_record);
        let query = { _id:id };
        let updatedTrend_record = await trend_recordService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedTrend_record){
          return message.successResponse({ data : updatedTrend_record });
        }
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const getTrend_recordById = async (query, body = {}) =>{
    try {
      if (query){
        let options = {};
        if (body && body.populate && body.populate.length) options.populate = body.populate;
        if (body && body.select && body.select.length) options.select = body.select;
        let result = await trend_recordService.getSingleDocument(query, options);
        if (result){
          return message.successResponse({ data: result });
        }
        return message.recordNotFound();
                 
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteTrend_record = async (data,id,loggedInUser) => {
    try {
      if (id){
        const query = { _id:id };
        let deletedTrend_record = await trend_recordService.findOneAndDeleteDocument(query);
        return message.successResponse({ data: deletedTrend_record });
                
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const removeEmpty = (obj) => {
    Object.entries(obj).forEach(([key,value])=>{
      if (value === undefined){
        delete obj[key];
      }
    });
    return obj;
  };

  return Object.freeze({
    addTrend_record,
    findAllTrend_record,
    getTrend_recordCount,
    getTrend_recordByAggregate,
    softDeleteManyTrend_record,
    bulkInsertTrend_record,
    bulkUpdateTrend_record,
    deleteManyTrend_record,
    softDeleteTrend_record,
    partialUpdateTrend_record,
    updateTrend_record,
    getTrend_recordById,
    deleteTrend_record,
    removeEmpty,
  });
}

module.exports = makeTrend_recordController;
