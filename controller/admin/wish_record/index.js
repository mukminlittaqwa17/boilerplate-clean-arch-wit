const db = require('mongoose');
const wish_recordModel = require('../../../model/wish_record')(db);
const {
  schemaKeys,updateSchemaKeys
} = require('../../../validation/wish_recordValidation');
const insertWish_recordValidator = require('../../../validation/genericValidator')(schemaKeys);
const updateWish_recordValidator = require('../../../validation/genericValidator')(updateSchemaKeys);
const makeWish_record = require('../../../entity/wish_record')({
  insertWish_recordValidator,
  updateWish_recordValidator
});
const wish_recordService = require('../../../services/mongoDbService')({
  model:wish_recordModel,
  makeWish_record
});
const makeWish_recordController = require('./wish_record');
const wish_recordController = makeWish_recordController({
  wish_recordService,
  makeWish_record
});
module.exports = wish_recordController;
