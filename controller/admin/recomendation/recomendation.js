const message = require('../../../utils/messages');

function makeRecomendationController ({
  recomendationService,makeRecomendation
})
{
  const addRecomendation = async ({
    data, loggedInUser
  }) => {
    try {
      let originalData = data;
      originalData.addedBy = loggedInUser.id.toString();
      const recomendation = makeRecomendation(originalData,'insertRecomendationValidator');
      let createdRecomendation = await recomendationService.createDocument(recomendation);
      return message.successResponse(
        { data :  createdRecomendation }
      );
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const findAllRecomendation = async ({
    data, loggedInUser
  }) => {
    try {
      let options = {};
      let query = {};
      let result;
      if (data.query !== undefined) {
        query = { ...data.query };
      }
      if (data.isCountOnly){
        result = await recomendationService.countDocument(query);
        if (result) {
          result = { totalRecords: result };  
          return message.successResponse(result);
        } else {
          return message.recordNotFound();
        }
      } else { 
        if (data.options !== undefined) {
          options = { ...data.options };
        }
        result = await recomendationService.getAllDocuments(query,options);
      }
      if (result.data){
        return message.successResponse({ data: result });
      } else {
        return message.recordNotFound();
      }
            
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getRecomendationCount = async (data) => {
    try {
      let where = {};
      if (data && data.where){
        where = data.where;
      }
      let result = await recomendationService.countDocument(where);
      result = { totalRecords:result };
      return message.successResponse({ data: result });
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getRecomendationByAggregate = async ({ data }) =>{
    try {
      if (data){
        let result = await recomendationService.getDocumentByAggregation(data);
        if (result && result.length){
          return message.successResponse({ data: result });
        }
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse(); 
    }
  };

  const softDeleteManyRecomendation = async (ids, loggedInUser) => {
    try {
      if (ids){
        const query = { _id:{ $in:ids } };
        const updateBody = { isDeleted: true, };
        let data = await recomendationService.bulkUpdate(query, updateBody);
        return message.successResponse({ data:data });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkInsertRecomendation = async ({
    body, loggedInUser
  }) => {
    try {
      let data = body.data;
      for (let i = 0;i < data.length;i++){
        data[i] = {
          ...data[i],
          addedBy:loggedInUser.id.toString(),
        };
      }
      const recomendationEntities = data.map((item)=>makeRecomendation(item,'insertRecomendationValidator'));
      const results = await recomendationService.bulkInsert(recomendationEntities);
      return message.successResponse({ data:results });
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkUpdateRecomendation = async (data, loggedInUser) => {
    try {
      if (data.filter && data.data){
        delete data.data['addedBy'];
        delete data.data['updatedBy'];
        data.data.updatedBy = loggedInUser.id;
        const recomendation = makeRecomendation(data.data,'updateRecomendationValidator');
        const filterData = removeEmpty(recomendation);
        let query = data.filter;
        const updatedRecomendations = await recomendationService.bulkUpdate(query,filterData);
        return message.successResponse({ data:updatedRecomendations });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteManyRecomendation = async (data, loggedInUser) => {
    try {
      if (data && data.ids){
        let ids = data.ids;
        const query = { '_id':{ '$in':ids } };
        let result = await recomendationService.deleteMany(query);
        return message.successResponse({ data:result });
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const softDeleteRecomendation = async (id,loggedInUser)=>{
    try {
      if (id){
        const query = { _id:id };
        const updateBody = { isDeleted: true, };
        let updatedRecomendation = await recomendationService.softDeleteByQuery(query, updateBody);
        return message.successResponse({ data:updatedRecomendation });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message: error.message });
      }
      return message.failureResponse();
    }
  };

  const partialUpdateRecomendation = async (data,id, loggedInUser) => {
    try {
      if (id && data){
        delete data['addedBy'];
        delete data['updatedBy'];
        recomendation.updatedBy = loggedInUser.id;
        const recomendation = makeRecomendation(data,'updateRecomendationValidator');            
        const filterData = removeEmpty(recomendation);
        const query = { _id:id };
        let updatedRecomendation = await recomendationService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedRecomendation){
          return message.successResponse({ data : updatedRecomendation });
        }
        else {
          return message.badRequest();
        }
      }
      else {
        return message.badRequest();
      }
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const updateRecomendation = async (data,id, loggedInUser) =>{
    try {
      delete data['addedBy'];
      delete data['updatedBy'];
      data.updatedBy = loggedInUser.id;
      if (id && data){
        const recomendation = makeRecomendation(data,'updateRecomendationValidator');
        const filterData = removeEmpty(recomendation);
        let query = { _id:id };
        let updatedRecomendation = await recomendationService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedRecomendation){
          return message.successResponse({ data : updatedRecomendation });
        }
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const getRecomendationById = async (query, body = {}) =>{
    try {
      if (query){
        let options = {};
        if (body && body.populate && body.populate.length) options.populate = body.populate;
        if (body && body.select && body.select.length) options.select = body.select;
        let result = await recomendationService.getSingleDocument(query, options);
        if (result){
          return message.successResponse({ data: result });
        }
        return message.recordNotFound();
                 
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteRecomendation = async (data,id,loggedInUser) => {
    try {
      if (id){
        const query = { _id:id };
        let deletedRecomendation = await recomendationService.findOneAndDeleteDocument(query);
        return message.successResponse({ data: deletedRecomendation });
                
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const removeEmpty = (obj) => {
    Object.entries(obj).forEach(([key,value])=>{
      if (value === undefined){
        delete obj[key];
      }
    });
    return obj;
  };

  return Object.freeze({
    addRecomendation,
    findAllRecomendation,
    getRecomendationCount,
    getRecomendationByAggregate,
    softDeleteManyRecomendation,
    bulkInsertRecomendation,
    bulkUpdateRecomendation,
    deleteManyRecomendation,
    softDeleteRecomendation,
    partialUpdateRecomendation,
    updateRecomendation,
    getRecomendationById,
    deleteRecomendation,
    removeEmpty,
  });
}

module.exports = makeRecomendationController;
