const db = require('mongoose');
const recomendationModel = require('../../../model/recomendation')(db);
const {
  schemaKeys,updateSchemaKeys
} = require('../../../validation/recomendationValidation');
const insertRecomendationValidator = require('../../../validation/genericValidator')(schemaKeys);
const updateRecomendationValidator = require('../../../validation/genericValidator')(updateSchemaKeys);
const makeRecomendation = require('../../../entity/recomendation')({
  insertRecomendationValidator,
  updateRecomendationValidator
});
const recomendationService = require('../../../services/mongoDbService')({
  model:recomendationModel,
  makeRecomendation
});
const makeRecomendationController = require('./recomendation');
const recomendationController = makeRecomendationController({
  recomendationService,
  makeRecomendation
});
module.exports = recomendationController;
