const message = require('../../../utils/messages');

function makeSourchrecomendationController ({
  sourchrecomendationService,makeSourchrecomendation
})
{
  const addSourchrecomendation = async ({
    data, loggedInUser
  }) => {
    try {
      let originalData = data;
      originalData.addedBy = loggedInUser.id.toString();
      const sourchrecomendation = makeSourchrecomendation(originalData,'insertSourchrecomendationValidator');
      let createdSourchrecomendation = await sourchrecomendationService.createDocument(sourchrecomendation);
      return message.successResponse(
        { data :  createdSourchrecomendation }
      );
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const findAllSourchrecomendation = async ({
    data, loggedInUser
  }) => {
    try {
      let options = {};
      let query = {};
      let result;
      if (data.query !== undefined) {
        query = { ...data.query };
      }
      if (data.isCountOnly){
        result = await sourchrecomendationService.countDocument(query);
        if (result) {
          result = { totalRecords: result };  
          return message.successResponse(result);
        } else {
          return message.recordNotFound();
        }
      } else { 
        if (data.options !== undefined) {
          options = { ...data.options };
        }
        result = await sourchrecomendationService.getAllDocuments(query,options);
      }
      if (result.data){
        return message.successResponse({ data: result });
      } else {
        return message.recordNotFound();
      }
            
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getSourchrecomendationCount = async (data) => {
    try {
      let where = {};
      if (data && data.where){
        where = data.where;
      }
      let result = await sourchrecomendationService.countDocument(where);
      result = { totalRecords:result };
      return message.successResponse({ data: result });
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getSourchrecomendationByAggregate = async ({ data }) =>{
    try {
      if (data){
        let result = await sourchrecomendationService.getDocumentByAggregation(data);
        if (result && result.length){
          return message.successResponse({ data: result });
        }
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse(); 
    }
  };

  const softDeleteManySourchrecomendation = async (ids, loggedInUser) => {
    try {
      if (ids){
        const deleteDependentService = require('../../../utils/deleteDependent');
        const query = { _id:{ $in:ids } };
        const updateBody = { isDeleted: true, };
        let result = await deleteDependentService.softDeleteSourchrecomendation(query, updateBody);
        return message.successResponse({ data:result });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkInsertSourchrecomendation = async ({
    body, loggedInUser
  }) => {
    try {
      let data = body.data;
      for (let i = 0;i < data.length;i++){
        data[i] = {
          ...data[i],
          addedBy:loggedInUser.id.toString(),
        };
      }
      const sourchrecomendationEntities = data.map((item)=>makeSourchrecomendation(item,'insertSourchrecomendationValidator'));
      const results = await sourchrecomendationService.bulkInsert(sourchrecomendationEntities);
      return message.successResponse({ data:results });
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkUpdateSourchrecomendation = async (data, loggedInUser) => {
    try {
      if (data.filter && data.data){
        delete data.data['addedBy'];
        delete data.data['updatedBy'];
        data.data.updatedBy = loggedInUser.id;
        const sourchrecomendation = makeSourchrecomendation(data.data,'updateSourchrecomendationValidator');
        const filterData = removeEmpty(sourchrecomendation);
        let query = data.filter;
        const updatedSourchrecomendations = await sourchrecomendationService.bulkUpdate(query,filterData);
        return message.successResponse({ data:updatedSourchrecomendations });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteManySourchrecomendation = async (data, loggedInUser) => {
    try {
      if (data && data.ids){
        const deleteDependentService = require('../../../utils/deleteDependent');
        let ids = data.ids;
        const query = { '_id':{ '$in':ids } };
        let result;
        if (data.isWarning){
          result = await deleteDependentService.countSourchrecomendation(query);
        } else {
          result = await deleteDependentService.deleteSourchrecomendation(query);
        }
        return message.successResponse({ data:result });
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const softDeleteSourchrecomendation = async (id,loggedInUser) => {
    try {
      const deleteDependentService = require('../../../utils/deleteDependent');
      const query = { _id:id };
      const updateBody = { isDeleted: true, };
      let result = await deleteDependentService.softDeleteSourchrecomendation(query, updateBody);
      return message.successResponse({ data:result });
            
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message: error.message });
      }
      return message.failureResponse();
    }
  };

  const partialUpdateSourchrecomendation = async (data,id, loggedInUser) => {
    try {
      if (id && data){
        delete data['addedBy'];
        delete data['updatedBy'];
        sourchrecomendation.updatedBy = loggedInUser.id;
        const sourchrecomendation = makeSourchrecomendation(data,'updateSourchrecomendationValidator');            
        const filterData = removeEmpty(sourchrecomendation);
        const query = { _id:id };
        let updatedSourchrecomendation = await sourchrecomendationService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedSourchrecomendation){
          return message.successResponse({ data : updatedSourchrecomendation });
        }
        else {
          return message.badRequest();
        }
      }
      else {
        return message.badRequest();
      }
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const updateSourchrecomendation = async (data,id, loggedInUser) =>{
    try {
      delete data['addedBy'];
      delete data['updatedBy'];
      data.updatedBy = loggedInUser.id;
      if (id && data){
        const sourchrecomendation = makeSourchrecomendation(data,'updateSourchrecomendationValidator');
        const filterData = removeEmpty(sourchrecomendation);
        let query = { _id:id };
        let updatedSourchrecomendation = await sourchrecomendationService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedSourchrecomendation){
          return message.successResponse({ data : updatedSourchrecomendation });
        }
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const getSourchrecomendationById = async (query, body = {}) =>{
    try {
      if (query){
        let options = {};
        if (body && body.populate && body.populate.length) options.populate = body.populate;
        if (body && body.select && body.select.length) options.select = body.select;
        let result = await sourchrecomendationService.getSingleDocument(query, options);
        if (result){
          return message.successResponse({ data: result });
        }
        return message.recordNotFound();
                 
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteSourchrecomendation = async (data,id,loggedInUser) => {
    try {
      const deleteDependentService = require('../../../utils/deleteDependent');
      let query = { _id:id };
      if (data.isWarning) {
        let all = await deleteDependentService.countSourchrecomendation(query);
        return message.successResponse({ data:all });
      } else {
        let result = await deleteDependentService.deleteSourchrecomendation(query);
        if (result){
          return message.successResponse({ data:result });
                    
        }
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message:error.message });
      }
      return message.failureResponse();
    }
  };

  const removeEmpty = (obj) => {
    Object.entries(obj).forEach(([key,value])=>{
      if (value === undefined){
        delete obj[key];
      }
    });
    return obj;
  };

  return Object.freeze({
    addSourchrecomendation,
    findAllSourchrecomendation,
    getSourchrecomendationCount,
    getSourchrecomendationByAggregate,
    softDeleteManySourchrecomendation,
    bulkInsertSourchrecomendation,
    bulkUpdateSourchrecomendation,
    deleteManySourchrecomendation,
    softDeleteSourchrecomendation,
    partialUpdateSourchrecomendation,
    updateSourchrecomendation,
    getSourchrecomendationById,
    deleteSourchrecomendation,
    removeEmpty,
  });
}

module.exports = makeSourchrecomendationController;
