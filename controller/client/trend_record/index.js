const db = require('mongoose');
const trend_recordModel = require('../../../model/trend_record')(db);
const {
  schemaKeys,updateSchemaKeys
} = require('../../../validation/trend_recordValidation');
const insertTrend_recordValidator = require('../../../validation/genericValidator')(schemaKeys);
const updateTrend_recordValidator = require('../../../validation/genericValidator')(updateSchemaKeys);
const makeTrend_record = require('../../../entity/trend_record')({
  insertTrend_recordValidator,
  updateTrend_recordValidator
});
const trend_recordService = require('../../../services/mongoDbService')({
  model:trend_recordModel,
  makeTrend_record
});
const makeTrend_recordController = require('./trend_record');
const trend_recordController = makeTrend_recordController({
  trend_recordService,
  makeTrend_record
});
module.exports = trend_recordController;
