const message = require('../../../utils/messages');

function makeKios_sehat_recordController ({
  kios_sehat_recordService,makeKios_sehat_record
})
{
  const addKios_sehat_record = async ({
    data, loggedInUser
  }) => {
    try {
      let originalData = data;
      originalData.addedBy = loggedInUser.id.toString();
      const kios_sehat_record = makeKios_sehat_record(originalData,'insertKios_sehat_recordValidator');
      let createdKios_sehat_record = await kios_sehat_recordService.createDocument(kios_sehat_record);
      return message.successResponse(
        { data :  createdKios_sehat_record }
      );
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const findAllKios_sehat_record = async ({
    data, loggedInUser
  }) => {
    try {
      let options = {};
      let query = {};
      let result;
      if (data.query !== undefined) {
        query = { ...data.query };
      }
      if (data.isCountOnly){
        result = await kios_sehat_recordService.countDocument(query);
        if (result) {
          result = { totalRecords: result };  
          return message.successResponse(result);
        } else {
          return message.recordNotFound();
        }
      } else { 
        if (data.options !== undefined) {
          options = { ...data.options };
        }
        result = await kios_sehat_recordService.getAllDocuments(query,options);
      }
      if (result.data){
        return message.successResponse({ data: result });
      } else {
        return message.recordNotFound();
      }
            
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getKios_sehat_recordCount = async (data) => {
    try {
      let where = {};
      if (data && data.where){
        where = data.where;
      }
      let result = await kios_sehat_recordService.countDocument(where);
      result = { totalRecords:result };
      return message.successResponse({ data: result });
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const getKios_sehat_recordByAggregate = async ({ data }) =>{
    try {
      if (data){
        let result = await kios_sehat_recordService.getDocumentByAggregation(data);
        if (result && result.length){
          return message.successResponse({ data: result });
        }
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse(); 
    }
  };

  const softDeleteManyKios_sehat_record = async (ids, loggedInUser) => {
    try {
      if (ids){
        const query = { _id:{ $in:ids } };
        const updateBody = { isDeleted: true, };
        let data = await kios_sehat_recordService.bulkUpdate(query, updateBody);
        return message.successResponse({ data:data });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkInsertKios_sehat_record = async ({
    body, loggedInUser
  }) => {
    try {
      let data = body.data;
      for (let i = 0;i < data.length;i++){
        data[i] = {
          ...data[i],
          addedBy:loggedInUser.id.toString(),
        };
      }
      const kios_sehat_recordEntities = data.map((item)=>makeKios_sehat_record(item,'insertKios_sehat_recordValidator'));
      const results = await kios_sehat_recordService.bulkInsert(kios_sehat_recordEntities);
      return message.successResponse({ data:results });
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      return message.failureResponse();
    }
  };

  const bulkUpdateKios_sehat_record = async (data, loggedInUser) => {
    try {
      if (data.filter && data.data){
        delete data.data['addedBy'];
        delete data.data['updatedBy'];
        data.data.updatedBy = loggedInUser.id;
        const kios_sehat_record = makeKios_sehat_record(data.data,'updateKios_sehat_recordValidator');
        const filterData = removeEmpty(kios_sehat_record);
        let query = data.filter;
        const updatedKios_sehat_records = await kios_sehat_recordService.bulkUpdate(query,filterData);
        return message.successResponse({ data:updatedKios_sehat_records });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteManyKios_sehat_record = async (data, loggedInUser) => {
    try {
      if (data && data.ids){
        let ids = data.ids;
        const query = { '_id':{ '$in':ids } };
        let result = await kios_sehat_recordService.deleteMany(query);
        return message.successResponse({ data:result });
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const softDeleteKios_sehat_record = async (id,loggedInUser)=>{
    try {
      if (id){
        const query = { _id:id };
        const updateBody = { isDeleted: true, };
        let updatedKios_sehat_record = await kios_sehat_recordService.softDeleteByQuery(query, updateBody);
        return message.successResponse({ data:updatedKios_sehat_record });
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message: error.message });
      }
      return message.failureResponse();
    }
  };

  const partialUpdateKios_sehat_record = async (data,id, loggedInUser) => {
    try {
      if (id && data){
        delete data['addedBy'];
        delete data['updatedBy'];
        kios_sehat_record.updatedBy = loggedInUser.id;
        const kios_sehat_record = makeKios_sehat_record(data,'updateKios_sehat_recordValidator');            
        const filterData = removeEmpty(kios_sehat_record);
        const query = { _id:id };
        let updatedKios_sehat_record = await kios_sehat_recordService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedKios_sehat_record){
          return message.successResponse({ data : updatedKios_sehat_record });
        }
        else {
          return message.badRequest();
        }
      }
      else {
        return message.badRequest();
      }
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const updateKios_sehat_record = async (data,id, loggedInUser) =>{
    try {
      delete data['addedBy'];
      delete data['updatedBy'];
      data.updatedBy = loggedInUser.id;
      console.log(data.updatedBy);
      if (id && data){
        const kios_sehat_record = makeKios_sehat_record(data,'updateKios_sehat_recordValidator');
        const filterData = removeEmpty(kios_sehat_record);
        let query = { _id:id };
        let updatedKios_sehat_record = await kios_sehat_recordService.findOneAndUpdateDocument(query,filterData,{ new:true });
        if (updatedKios_sehat_record){
          return message.successResponse({ data : updatedKios_sehat_record });
        }
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message : error.message });
      }
      console.log(error);
      return message.failureResponse();
    }
  };

  const getKios_sehat_recordById = async (query, body = {}) =>{
    try {
      if (query){
        let options = {};
        if (body && body.populate && body.populate.length) options.populate = body.populate;
        if (body && body.select && body.select.length) options.select = body.select;
        let result = await kios_sehat_recordService.getSingleDocument(query, options);
        if (result){
          return message.successResponse({ data: result });
        }
        return message.recordNotFound();
                 
      }
      return message.badRequest();
    }
    catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const deleteKios_sehat_record = async (data,id,loggedInUser) => {
    try {
      if (id){
        const query = { _id:id };
        let deletedKios_sehat_record = await kios_sehat_recordService.findOneAndDeleteDocument(query);
        return message.successResponse({ data: deletedKios_sehat_record });
                
      }
      return message.badRequest();
    } catch (error){
      if (error.name === 'ValidationError'){
        return message.inValidParam({ message :error.message });
      }
      return message.failureResponse();
    }
  };

  const removeEmpty = (obj) => {
    Object.entries(obj).forEach(([key,value])=>{
      if (value === undefined){
        delete obj[key];
      }
    });
    return obj;
  };

  return Object.freeze({
    addKios_sehat_record,
    findAllKios_sehat_record,
    getKios_sehat_recordCount,
    getKios_sehat_recordByAggregate,
    softDeleteManyKios_sehat_record,
    bulkInsertKios_sehat_record,
    bulkUpdateKios_sehat_record,
    deleteManyKios_sehat_record,
    softDeleteKios_sehat_record,
    partialUpdateKios_sehat_record,
    updateKios_sehat_record,
    getKios_sehat_recordById,
    deleteKios_sehat_record,
    removeEmpty,
  });
}

module.exports = makeKios_sehat_recordController;
