const db = require('mongoose');
const kios_sehat_recordModel = require('../../../model/kios_sehat_record')(db);
const {
  schemaKeys,updateSchemaKeys
} = require('../../../validation/kios_sehat_recordValidation');
const insertKios_sehat_recordValidator = require('../../../validation/genericValidator')(schemaKeys);
const updateKios_sehat_recordValidator = require('../../../validation/genericValidator')(updateSchemaKeys);
const makeKios_sehat_record = require('../../../entity/kios_sehat_record')({
  insertKios_sehat_recordValidator,
  updateKios_sehat_recordValidator
});
const kios_sehat_recordService = require('../../../services/mongoDbService')({
  model:kios_sehat_recordModel,
  makeKios_sehat_record
});
const makeKios_sehat_recordController = require('./kios_sehat_record');
const kios_sehat_recordController = makeKios_sehat_recordController({
  kios_sehat_recordService,
  makeKios_sehat_record
});
module.exports = kios_sehat_recordController;
