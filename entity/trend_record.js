
function buildMakeTrend_record ({
  insertTrend_recordValidator,updateTrend_recordValidator
}){
  return function makeTrend_record (data,validatorName){
    let isValid = '';
    switch (validatorName){
    case 'insertTrend_recordValidator':
      isValid = insertTrend_recordValidator(data);
      break;

    case 'updateTrend_recordValidator':
      isValid = updateTrend_recordValidator(data);  
      break; 
    }
    if (isValid.error){
      throw ({
        name:'ValidationError',
        message:`Invalid data in Trend_record entity. ${isValid.error}`
      });
    }
      
    return {
      user:data.user,
      healtstatus:data.healtstatus,
      oldhealtstatus:data.oldhealtstatus,
      prediction:data.prediction,
      predictionnumber:data.predictionnumber,
      sehatprediction:data.sehatprediction,
      resikorendahprediction:data.resikorendahprediction,
      resikotinggiprediction:data.resikotinggiprediction,
      sakitprediction:data.sakitprediction,
      heartrate:data.heartrate,
      heartratestatus:data.heartratestatus,
      bloodoxygen:data.bloodoxygen,
      bloodoxygenstatus:data.bloodoxygenstatus,
      temperatur:data.temperatur,
      temperaturstatus:data.temperaturstatus,
      recomenadtioncode:data.recomenadtioncode,
      createdAt:data.createdAt,
      updatedAt:data.updatedAt,
      isDeleted:data.isDeleted,
      isActive:data.isActive,
      addedBy:data.addedBy,
    };
  };
}
module.exports =  buildMakeTrend_record;
