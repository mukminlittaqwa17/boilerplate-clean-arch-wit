
function buildMakeUser ({
  insertUserValidator,updateUserValidator
}){
  return function makeUser (data,validatorName){
    let isValid = '';
    switch (validatorName){
    case 'insertUserValidator':
      isValid = insertUserValidator(data);
      break;

    case 'updateUserValidator':
      isValid = updateUserValidator(data);  
      break; 
    }
    if (isValid.error){
      throw ({
        name:'ValidationError',
        message:`Invalid data in User entity. ${isValid.error}`
      });
    }
      
    return {
      fullname:data.fullname,
      gender:data.gender,
      dateofbird:data.dateofbird,
      email:data.email,
      password:data.password,
      address:data.address,
      phonenumber:data.phonenumber,
      bodyweight:data.bodyweight,
      height:data.height,
      device:data.device,
      nik:data.nik,
      nip:data.nip,
      photo:data.photo,
      isActive:data.isActive,
      createdAt:data.createdAt,
      updatedAt:data.updatedAt,
      addedBy:data.addedBy,
      updatedBy:data.updatedBy,
      mobileNo:data.mobileNo,
      username:data.username,
      isDeleted:data.isDeleted,
      role:data.role,
      resetPasswordLink:data.resetPasswordLink,
      loginRetryLimit:data.loginRetryLimit,
      loginReactiveTime:data.loginReactiveTime,
    };
  };
}
module.exports =  buildMakeUser;
