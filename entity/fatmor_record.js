
function buildMakeFatmor_record ({
  insertFatmor_recordValidator,updateFatmor_recordValidator
}){
  return function makeFatmor_record (data,validatorName){
    let isValid = '';
    switch (validatorName){
    case 'insertFatmor_recordValidator':
      isValid = insertFatmor_recordValidator(data);
      break;

    case 'updateFatmor_recordValidator':
      isValid = updateFatmor_recordValidator(data);  
      break; 
    }
    if (isValid.error){
      throw ({
        name:'ValidationError',
        message:`Invalid data in Fatmor_record entity. ${isValid.error}`
      });
    }
      
    return {
      user:data.user,
      photo:data.photo,
      temperatur:data.temperatur,
      absenstype:data.absenstype,
      createdAt:data.createdAt,
      updatedAt:data.updatedAt,
      isDeleted:data.isDeleted,
      isActive:data.isActive,
      addedBy:data.addedBy,
    };
  };
}
module.exports =  buildMakeFatmor_record;
