
function buildMakeRecomendation ({
  insertRecomendationValidator,updateRecomendationValidator
}){
  return function makeRecomendation (data,validatorName){
    let isValid = '';
    switch (validatorName){
    case 'insertRecomendationValidator':
      isValid = insertRecomendationValidator(data);
      break;

    case 'updateRecomendationValidator':
      isValid = updateRecomendationValidator(data);  
      break; 
    }
    if (isValid.error){
      throw ({
        name:'ValidationError',
        message:`Invalid data in Recomendation entity. ${isValid.error}`
      });
    }
      
    return {
      recomendation:data.recomendation,
      createby:data.createby,
      sourch:data.sourch,
      reccode:data.reccode,
      createdAt:data.createdAt,
      updatedAt:data.updatedAt,
      isDeleted:data.isDeleted,
      isActive:data.isActive,
      addedBy:data.addedBy,
    };
  };
}
module.exports =  buildMakeRecomendation;
