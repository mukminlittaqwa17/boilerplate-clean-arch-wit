
function buildMakeWish_record ({
  insertWish_recordValidator,updateWish_recordValidator
}){
  return function makeWish_record (data,validatorName){
    let isValid = '';
    switch (validatorName){
    case 'insertWish_recordValidator':
      isValid = insertWish_recordValidator(data);
      break;

    case 'updateWish_recordValidator':
      isValid = updateWish_recordValidator(data);  
      break; 
    }
    if (isValid.error){
      throw ({
        name:'ValidationError',
        message:`Invalid data in Wish_record entity. ${isValid.error}`
      });
    }
      
    return {
      user:data.user,
      temperatur:data.temperatur,
      bloodoxygen:data.bloodoxygen,
      heartrate:data.heartrate,
      createdAt:data.createdAt,
      updatedBy:data.updatedBy,
      isDeleted:data.isDeleted,
      isActive:data.isActive,
      addedBy:data.addedBy,
    };
  };
}
module.exports =  buildMakeWish_record;
