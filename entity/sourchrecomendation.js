
function buildMakeSourchrecomendation ({
  insertSourchrecomendationValidator,updateSourchrecomendationValidator
}){
  return function makeSourchrecomendation (data,validatorName){
    let isValid = '';
    switch (validatorName){
    case 'insertSourchrecomendationValidator':
      isValid = insertSourchrecomendationValidator(data);
      break;

    case 'updateSourchrecomendationValidator':
      isValid = updateSourchrecomendationValidator(data);  
      break; 
    }
    if (isValid.error){
      throw ({
        name:'ValidationError',
        message:`Invalid data in Sourchrecomendation entity. ${isValid.error}`
      });
    }
      
    return {
      name:data.name,
      yearpublication:data.yearpublication,
      title:data.title,
      link:data.link,
      createdAt:data.createdAt,
      updatedAt:data.updatedAt,
      isDeleted:data.isDeleted,
      isActive:data.isActive,
      addedBy:data.addedBy,
    };
  };
}
module.exports =  buildMakeSourchrecomendation;
