
function buildMakeKios_sehat_record ({
  insertKios_sehat_recordValidator,updateKios_sehat_recordValidator
}){
  return function makeKios_sehat_record (data,validatorName){
    let isValid = '';
    switch (validatorName){
    case 'insertKios_sehat_recordValidator':
      isValid = insertKios_sehat_recordValidator(data);
      break;

    case 'updateKios_sehat_recordValidator':
      isValid = updateKios_sehat_recordValidator(data);  
      break; 
    }
    if (isValid.error){
      throw ({
        name:'ValidationError',
        message:`Invalid data in Kios_sehat_record entity. ${isValid.error}`
      });
    }
      
    return {
      user:data.user,
      temperatur:data.temperatur,
      bloodoxygen:data.bloodoxygen,
      heartrate:data.heartrate,
      glucose:data.glucose,
      bodyweight:data.bodyweight,
      height:data.height,
      createdAt:data.createdAt,
      updatedBy:data.updatedBy,
      isDeleted:data.isDeleted,
      isActive:data.isActive,
      addedBy:data.addedBy,
    };
  };
}
module.exports =  buildMakeKios_sehat_record;
