const express =  require('express');
const router =  express.Router();
router.use('/device/auth',require('./auth'));
router.use(require('./sourchrecomendationRoutes'));
router.use(require('./recomendationRoutes'));
router.use(require('./trend_recordRoutes'));
router.use(require('./fatmor_recordRoutes'));
router.use(require('./wish_recordRoutes'));
router.use(require('./kios_sehat_recordRoutes'));
router.use(require('./userRoutes'));

module.exports = router;
