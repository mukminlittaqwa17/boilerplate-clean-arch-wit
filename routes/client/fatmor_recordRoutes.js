const express = require('express');
const router = express.Router();
const fatmor_recordController = require('../../controller/client/fatmor_record');
const adaptRequest = require('../../helpers/adaptRequest');
const sendResponse = require('../../helpers/sendResponse');
const auth = require('../../middleware/auth');
const checkRolePermission = require('../../middleware/checkRolePermission');

router.post('/client/api/v1/fatmor_record/create',auth(...[ 'createByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.addFatmor_record({
    data: req.body,
    loggedInUser:req.user
  }).then((result)=>{
    sendResponse(res, result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.post('/client/api/v1/fatmor_record/list',auth(...[ 'getAllByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);    
  fatmor_recordController.findAllFatmor_record({
    data: req.body,
    loggedInUser:req.user
  }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.route('/client/api/v1/fatmor_record/count').post(auth(...[ 'getCountByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.getFatmor_recordCount(req.body).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.route('/client/api/v1/fatmor_record/aggregate').post(auth(...[ 'aggregateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.getFatmor_recordByAggregate({ data:req.body }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/fatmor_record/softDeleteMany',auth(...[ 'softDeleteManyByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.softDeleteManyFatmor_record(req.body.ids,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.post('/client/api/v1/fatmor_record/addBulk',auth(...[ 'addBulkByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  fatmor_recordController.bulkInsertFatmor_record({
    body: req.body,
    loggedInUser: req.user
  }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/fatmor_record/updateBulk',auth(...[ 'updateBulkByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.bulkUpdateFatmor_record(req.body,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
}); 

router.delete('/client/api/v1/fatmor_record/deleteMany',auth(...[ 'deleteManyByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.deleteManyFatmor_record(req.body,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/fatmor_record/softDelete/:id',auth(...[ 'softDeleteByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.softDeleteFatmor_record(req.pathParams.id,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/fatmor_record/partial-update/:id',auth(...[ 'partialUpdateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.partialUpdateFatmor_record(req.body,req.pathParams.id,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});   

router.put('/client/api/v1/fatmor_record/update/:id',auth(...[ 'updateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.updateFatmor_record(req.body,req.pathParams.id,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});   

router.get('/client/api/v1/fatmor_record/:id',auth(...[ 'getByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.getFatmor_recordById({ _id: req.pathParams.id }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});
router.post('/client/api/v1/fatmor_record/:id',auth(...[ 'getByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.getFatmor_recordById({ _id: req.pathParams.id }, req.body).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.delete('/client/api/v1/fatmor_record/delete/:id',auth(...[ 'deleteByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  fatmor_recordController.deleteFatmor_record(req.body,req.pathParams.id,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

module.exports = router;