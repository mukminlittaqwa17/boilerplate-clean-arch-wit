const express = require('express');
const router = express.Router();
const wish_recordController = require('../../controller/client/wish_record');
const adaptRequest = require('../../helpers/adaptRequest');
const sendResponse = require('../../helpers/sendResponse');
const auth = require('../../middleware/auth');
const checkRolePermission = require('../../middleware/checkRolePermission');

router.post('/client/api/v1/wish_record/create',auth(...[ 'createByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.addWish_record({
    data: req.body,
    loggedInUser:req.user
  }).then((result)=>{
    sendResponse(res, result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.post('/client/api/v1/wish_record/list',auth(...[ 'getAllByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);    
  wish_recordController.findAllWish_record({
    data: req.body,
    loggedInUser:req.user
  }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.route('/client/api/v1/wish_record/count').post(auth(...[ 'getCountByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.getWish_recordCount(req.body).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.route('/client/api/v1/wish_record/aggregate').post(auth(...[ 'aggregateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.getWish_recordByAggregate({ data:req.body }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/wish_record/softDeleteMany',auth(...[ 'softDeleteManyByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.softDeleteManyWish_record(req.body.ids,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.post('/client/api/v1/wish_record/addBulk',auth(...[ 'addBulkByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  wish_recordController.bulkInsertWish_record({
    body: req.body,
    loggedInUser: req.user
  }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/wish_record/updateBulk',auth(...[ 'updateBulkByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.bulkUpdateWish_record(req.body,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
}); 

router.delete('/client/api/v1/wish_record/deleteMany',auth(...[ 'deleteManyByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.deleteManyWish_record(req.body,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/wish_record/softDelete/:id',auth(...[ 'softDeleteByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.softDeleteWish_record(req.pathParams.id,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/wish_record/partial-update/:id',auth(...[ 'partialUpdateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.partialUpdateWish_record(req.body,req.pathParams.id,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});   

router.put('/client/api/v1/wish_record/update/:id',auth(...[ 'updateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.updateWish_record(req.body,req.pathParams.id,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});   

router.get('/client/api/v1/wish_record/:id',auth(...[ 'getByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.getWish_recordById({ _id: req.pathParams.id }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});
router.post('/client/api/v1/wish_record/:id',auth(...[ 'getByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.getWish_recordById({ _id: req.pathParams.id }, req.body).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.delete('/client/api/v1/wish_record/delete/:id',auth(...[ 'deleteByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  wish_recordController.deleteWish_record(req.body,req.pathParams.id,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

module.exports = router;