const express = require('express');
const router = express.Router();
const recomendationController = require('../../controller/client/recomendation');
const adaptRequest = require('../../helpers/adaptRequest');
const sendResponse = require('../../helpers/sendResponse');
const auth = require('../../middleware/auth');
const checkRolePermission = require('../../middleware/checkRolePermission');

router.post('/client/api/v1/recomendation/create',auth(...[ 'createByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.addRecomendation({
    data: req.body,
    loggedInUser:req.user
  }).then((result)=>{
    sendResponse(res, result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.post('/client/api/v1/recomendation/list',auth(...[ 'getAllByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);    
  recomendationController.findAllRecomendation({
    data: req.body,
    loggedInUser:req.user
  }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.route('/client/api/v1/recomendation/count').post(auth(...[ 'getCountByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.getRecomendationCount(req.body).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.route('/client/api/v1/recomendation/aggregate').post(auth(...[ 'aggregateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.getRecomendationByAggregate({ data:req.body }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/recomendation/softDeleteMany',auth(...[ 'softDeleteManyByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.softDeleteManyRecomendation(req.body.ids,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.post('/client/api/v1/recomendation/addBulk',auth(...[ 'addBulkByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  recomendationController.bulkInsertRecomendation({
    body: req.body,
    loggedInUser: req.user
  }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/recomendation/updateBulk',auth(...[ 'updateBulkByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.bulkUpdateRecomendation(req.body,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
}); 

router.delete('/client/api/v1/recomendation/deleteMany',auth(...[ 'deleteManyByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.deleteManyRecomendation(req.body,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/recomendation/softDelete/:id',auth(...[ 'softDeleteByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.softDeleteRecomendation(req.pathParams.id,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/recomendation/partial-update/:id',auth(...[ 'partialUpdateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.partialUpdateRecomendation(req.body,req.pathParams.id,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});   

router.put('/client/api/v1/recomendation/update/:id',auth(...[ 'updateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.updateRecomendation(req.body,req.pathParams.id,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});   

router.get('/client/api/v1/recomendation/:id',auth(...[ 'getByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.getRecomendationById({ _id: req.pathParams.id }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});
router.post('/client/api/v1/recomendation/:id',auth(...[ 'getByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.getRecomendationById({ _id: req.pathParams.id }, req.body).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.delete('/client/api/v1/recomendation/delete/:id',auth(...[ 'deleteByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  recomendationController.deleteRecomendation(req.body,req.pathParams.id,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

module.exports = router;