const express = require('express');
const router = express.Router();
const kios_sehat_recordController = require('../../controller/client/kios_sehat_record');
const adaptRequest = require('../../helpers/adaptRequest');
const sendResponse = require('../../helpers/sendResponse');
const auth = require('../../middleware/auth');
const checkRolePermission = require('../../middleware/checkRolePermission');

router.post('/client/api/v1/kios_sehat_record/create',auth(...[ 'createByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.addKios_sehat_record({
    data: req.body,
    loggedInUser:req.user
  }).then((result)=>{
    sendResponse(res, result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.post('/client/api/v1/kios_sehat_record/list',auth(...[ 'getAllByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);    
  kios_sehat_recordController.findAllKios_sehat_record({
    data: req.body,
    loggedInUser:req.user
  }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.route('/client/api/v1/kios_sehat_record/count').post(auth(...[ 'getCountByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.getKios_sehat_recordCount(req.body).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.route('/client/api/v1/kios_sehat_record/aggregate').post(auth(...[ 'aggregateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.getKios_sehat_recordByAggregate({ data:req.body }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/kios_sehat_record/softDeleteMany',auth(...[ 'softDeleteManyByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.softDeleteManyKios_sehat_record(req.body.ids,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.post('/client/api/v1/kios_sehat_record/addBulk',auth(...[ 'addBulkByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  kios_sehat_recordController.bulkInsertKios_sehat_record({
    body: req.body,
    loggedInUser: req.user
  }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/kios_sehat_record/updateBulk',auth(...[ 'updateBulkByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.bulkUpdateKios_sehat_record(req.body,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
}); 

router.delete('/client/api/v1/kios_sehat_record/deleteMany',auth(...[ 'deleteManyByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.deleteManyKios_sehat_record(req.body,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/kios_sehat_record/softDelete/:id',auth(...[ 'softDeleteByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.softDeleteKios_sehat_record(req.pathParams.id,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.put('/client/api/v1/kios_sehat_record/partial-update/:id',auth(...[ 'partialUpdateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.partialUpdateKios_sehat_record(req.body,req.pathParams.id,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});   

router.put('/client/api/v1/kios_sehat_record/update/:id',auth(...[ 'updateByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.updateKios_sehat_record(req.body,req.pathParams.id,req.user
  ).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});   

router.get('/client/api/v1/kios_sehat_record/:id',auth(...[ 'getByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.getKios_sehat_recordById({ _id: req.pathParams.id }).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});
router.post('/client/api/v1/kios_sehat_record/:id',auth(...[ 'getByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.getKios_sehat_recordById({ _id: req.pathParams.id }, req.body).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

router.delete('/client/api/v1/kios_sehat_record/delete/:id',auth(...[ 'deleteByAdminInClientPlatform' ]),checkRolePermission,(req,res,next)=>{
  req = adaptRequest(req);
  kios_sehat_recordController.deleteKios_sehat_record(req.body,req.pathParams.id,req.user).then((result)=>{
    sendResponse(res,result);
  })
    .catch((error) => {
      sendResponse(res,error);
    });
});

module.exports = router;