const express = require('express');
const router =  express.Router();

router.use(require('./device/index'));
router.use(require('./client/index'));
router.use(require('./admin/index'));

module.exports = router;
