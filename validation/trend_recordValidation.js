const joi = require('joi');

exports.schemaKeys = joi.object({
  user: joi.string().regex(/^[0-9a-fA-F]{24}$/).allow(null).allow(''),
  healtstatus: joi.string().allow(null).allow(''),
  oldhealtstatus: joi.string().allow(null).allow(''),
  prediction: joi.string().allow(null).allow(''),
  predictionnumber: joi.string().allow(null).allow(''),
  sehatprediction: joi.string().allow(null).allow(''),
  resikorendahprediction: joi.string().allow(null).allow(''),
  resikotinggiprediction: joi.string().allow(null).allow(''),
  sakitprediction: joi.string().allow(null).allow(''),
  heartrate: joi.number().integer().allow(0),
  heartratestatus: joi.string().allow(null).allow(''),
  bloodoxygen: joi.number().integer().allow(0),
  bloodoxygenstatus: joi.string().allow(null).allow(''),
  temperatur: joi.string().allow(null).allow(''),
  temperaturstatus: joi.string().allow(null).allow(''),
  recomenadtioncode: joi.string().allow(null).allow(''),
  isActive: joi.boolean(),
  isDeleted: joi.boolean()
}).unknown(true);
exports.updateSchemaKeys = joi.object({
  user: joi.string().regex(/^[0-9a-fA-F]{24}$/).allow(null).allow(''),
  healtstatus: joi.string().allow(null).allow(''),
  oldhealtstatus: joi.string().allow(null).allow(''),
  prediction: joi.string().allow(null).allow(''),
  predictionnumber: joi.string().allow(null).allow(''),
  sehatprediction: joi.string().allow(null).allow(''),
  resikorendahprediction: joi.string().allow(null).allow(''),
  resikotinggiprediction: joi.string().allow(null).allow(''),
  sakitprediction: joi.string().allow(null).allow(''),
  heartrate: joi.number().integer().allow(0),
  heartratestatus: joi.string().allow(null).allow(''),
  bloodoxygen: joi.number().integer().allow(0),
  bloodoxygenstatus: joi.string().allow(null).allow(''),
  temperatur: joi.string().allow(null).allow(''),
  temperaturstatus: joi.string().allow(null).allow(''),
  recomenadtioncode: joi.string().allow(null).allow(''),
  isActive: joi.boolean(),
  isDeleted: joi.boolean(),
  _id: joi.string().regex(/^[0-9a-fA-F]{24}$/)
}).unknown(true);
