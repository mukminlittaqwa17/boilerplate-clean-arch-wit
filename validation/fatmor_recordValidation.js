const joi = require('joi');

exports.schemaKeys = joi.object({
  user: joi.string().regex(/^[0-9a-fA-F]{24}$/).allow(null).allow(''),
  photo: joi.string().required(),
  temperatur: joi.string().required(),
  absenstype: joi.string().required(),
  isActive: joi.boolean(),
  isDeleted: joi.boolean()
}).unknown(true);
exports.updateSchemaKeys = joi.object({
  user: joi.string().regex(/^[0-9a-fA-F]{24}$/).allow(null).allow(''),
  photo: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  temperatur: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  absenstype: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  isActive: joi.boolean(),
  isDeleted: joi.boolean(),
  _id: joi.string().regex(/^[0-9a-fA-F]{24}$/)
}).unknown(true);
