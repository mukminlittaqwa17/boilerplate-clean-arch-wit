const joi = require('joi');

exports.schemaKeys = joi.object({
  user: joi.string().regex(/^[0-9a-fA-F]{24}$/).allow(null).allow(''),
  temperatur: joi.string().required(),
  bloodoxygen: joi.string().required(),
  heartrate: joi.string().required(),
  isActive: joi.boolean(),
  isDeleted: joi.boolean()
}).unknown(true);
exports.updateSchemaKeys = joi.object({
  user: joi.string().regex(/^[0-9a-fA-F]{24}$/).allow(null).allow(''),
  temperatur: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  bloodoxygen: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  heartrate: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  isActive: joi.boolean(),
  isDeleted: joi.boolean(),
  _id: joi.string().regex(/^[0-9a-fA-F]{24}$/)
}).unknown(true);
