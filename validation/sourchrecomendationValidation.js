const joi = require('joi');

exports.schemaKeys = joi.object({
  name: joi.string().allow(null).allow(''),
  yearpublication: joi.date().allow(null).allow(''),
  title: joi.string().allow(null).allow(''),
  link: joi.string().allow(null).allow(''),
  isActive: joi.boolean(),
  isDeleted: joi.boolean()
}).unknown(true);
exports.updateSchemaKeys = joi.object({
  name: joi.string().allow(null).allow(''),
  yearpublication: joi.date().allow(null).allow(''),
  title: joi.string().allow(null).allow(''),
  link: joi.string().allow(null).allow(''),
  isActive: joi.boolean(),
  isDeleted: joi.boolean(),
  _id: joi.string().regex(/^[0-9a-fA-F]{24}$/)
}).unknown(true);
