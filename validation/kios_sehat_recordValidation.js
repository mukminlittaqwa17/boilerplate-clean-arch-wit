const joi = require('joi');

exports.schemaKeys = joi.object({
  user: joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
  temperatur: joi.number().integer().required(),
  bloodoxygen: joi.number().integer().required(),
  heartrate: joi.string().allow(null).allow(''),
  glucose: joi.string().allow(null).allow(''),
  bodyweight: joi.number().integer().allow(0),
  height: joi.number().integer().allow(0),
  isActive: joi.boolean(),
  isDeleted: joi.boolean()
}).unknown(true);
exports.updateSchemaKeys = joi.object({
  user: joi.string().regex(/^[0-9a-fA-F]{24}$/).when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  temperatur: joi.number().integer().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  bloodoxygen: joi.number().integer().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  heartrate: joi.string().allow(null).allow(''),
  glucose: joi.string().allow(null).allow(''),
  bodyweight: joi.number().integer().allow(0),
  height: joi.number().integer().allow(0),
  isActive: joi.boolean(),
  isDeleted: joi.boolean(),
  _id: joi.string().regex(/^[0-9a-fA-F]{24}$/)
}).unknown(true);
