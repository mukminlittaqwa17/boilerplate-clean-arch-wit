const joi = require('joi');

const { USER_ROLE } = require('../constants/authConstant');
const { convertObjectToEnum } = require('../utils/common');   
exports.schemaKeys = joi.object({
  fullname: joi.string().required(),
  gender: joi.string().allow(null).allow(''),
  dateofbird: joi.string().allow(null).allow(''),
  email: joi.string().required(),
  password: joi.string().required(),
  address: joi.string().required(),
  phonenumber: joi.string().required(),
  bodyweight: joi.number().integer().allow(0),
  height: joi.number().integer().allow(0),
  device: joi.string().allow(null).allow(''),
  nik: joi.number().integer().allow(0),
  nip: joi.number().integer().allow(0),
  photo: joi.string().allow(null).allow(''),
  isActive: joi.boolean(),
  mobileNo: joi.string().allow(null).allow(''),
  username: joi.string().allow(null).allow(''),
  role: joi.number().integer().valid(...convertObjectToEnum(USER_ROLE)).allow(0),
  resetPasswordLink: joi.object({
    code:joi.string(),
    expireTime:joi.date()
  }),
  isDeleted: joi.boolean()
}).unknown(true);
exports.updateSchemaKeys = joi.object({
  fullname: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  gender: joi.string().allow(null).allow(''),
  dateofbird: joi.string().allow(null).allow(''),
  email: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  password: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  address: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  phonenumber: joi.string().when({
    is:joi.exist(),
    then:joi.required(),
    otherwise:joi.optional()
  }),
  bodyweight: joi.number().integer().allow(0),
  height: joi.number().integer().allow(0),
  device: joi.string().allow(null).allow(''),
  nik: joi.number().integer().allow(0),
  nip: joi.number().integer().allow(0),
  photo: joi.string().allow(null).allow(''),
  isActive: joi.boolean(),
  mobileNo: joi.string().allow(null).allow(''),
  username: joi.string().allow(null).allow(''),
  role: joi.number().integer().valid(...convertObjectToEnum(USER_ROLE)).allow(0),
  resetPasswordLink: joi.object({
    code:joi.string(),
    expireTime:joi.date()
  }),
  isDeleted: joi.boolean(),
  _id: joi.string().regex(/^[0-9a-fA-F]{24}$/)
}).unknown(true);
