function makeModel (mongoose,...dependencies){
    
  if (!mongoose.models.kios_sehat_record){
    const mongoosePaginate = require('mongoose-paginate-v2');
    const idvalidator = require('mongoose-id-validator');
    const myCustomLabels = {
      totalDocs: 'itemCount',
      docs: 'data',
      limit: 'perPage',
      page: 'currentPage',
      nextPage: 'next',
      prevPage: 'prev',
      totalPages: 'pageCount',
      pagingCounter: 'slNo',
      meta: 'paginator',
    };
    mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
    const Schema = mongoose.Schema;
    const schema = new Schema(
      {
        user:{
          type:Schema.Types.ObjectId,
          ref:'user',
          required:true
        },
        temperatur:{
          type:Number,
          required:false
        },
        bloodoxygen:{
          type:Number,
          required:false
        },
        heartrate:{ type:String },
        glucose:{ type:String },
        bodyweight:{ type:Number },
        height:{ type:Number },
        createdAt:{ type:Date },
        updatedBy:{
          type:Schema.Types.ObjectId,
          ref:'user'
        },
        isDeleted:{ type:Boolean },
        isActive:{ type:Boolean },
        addedBy:{
          type:Schema.Types.ObjectId,
          ref:'user'
        }
      },
      {
        timestamps: {
          createdAt: 'createdAt',
          updatedAt: 'updatedAt' 
        } 
      }
    );
    
    schema.pre('save', async function (next) {
      this.isDeleted = false;
      this.isActive = true;
      next();
    });

    schema.pre('insertMany', async function (next, docs) {
      if (docs && docs.length){
        for (let index = 0; index < docs.length; index++) {
          const element = docs[index];
          element.isDeleted = false;
          element.isActive = true;
        }
      }
      next();
    });

    schema.method('toJSON', function () {
      const {
        __v, ...object 
      } = this.toObject({ virtuals:true });
      object.id = object._id;
      return object;
    });
    schema.plugin(mongoosePaginate);
    schema.plugin(idvalidator);

    const kios_sehat_record = mongoose.model('kios_sehat_record',schema,'kios_sehat_record');
    return kios_sehat_record;
  }
  else {
    return mongoose.models.kios_sehat_record;
  }
}
module.exports = makeModel;