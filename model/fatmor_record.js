function makeModel (mongoose,...dependencies){
    
  if (!mongoose.models.fatmor_record){
    const mongoosePaginate = require('mongoose-paginate-v2');
    const idvalidator = require('mongoose-id-validator');
    const myCustomLabels = {
      totalDocs: 'itemCount',
      docs: 'data',
      limit: 'perPage',
      page: 'currentPage',
      nextPage: 'next',
      prevPage: 'prev',
      totalPages: 'pageCount',
      pagingCounter: 'slNo',
      meta: 'paginator',
    };
    mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
    const Schema = mongoose.Schema;
    const schema = new Schema(
      {
        user:{
          type:Schema.Types.ObjectId,
          ref:'user'
        },
        photo:{
          type:String,
          required:true
        },
        temperatur:{
          type:String,
          required:true
        },
        absenstype:{
          type:String,
          required:false
        },
        createdAt:{
          type:Date,
          required:true
        },
        updatedAt:{
          type:Date,
          required:true
        },
        isDeleted:{ type:Boolean },
        isActive:{ type:Boolean },
        addedBy:{
          type:Schema.Types.ObjectId,
          ref:'user'
        }
      },
      {
        timestamps: {
          createdAt: 'createdAt',
          updatedAt: 'updatedAt' 
        } 
      }
    );
    
    schema.pre('save', async function (next) {
      this.isDeleted = false;
      this.isActive = true;
      next();
    });

    schema.pre('insertMany', async function (next, docs) {
      if (docs && docs.length){
        for (let index = 0; index < docs.length; index++) {
          const element = docs[index];
          element.isDeleted = false;
          element.isActive = true;
        }
      }
      next();
    });

    schema.method('toJSON', function () {
      const {
        __v, ...object 
      } = this.toObject({ virtuals:true });
      object.id = object._id;
      return object;
    });
    schema.plugin(mongoosePaginate);
    schema.plugin(idvalidator);

    const fatmor_record = mongoose.model('fatmor_record',schema,'fatmor_record');
    return fatmor_record;
  }
  else {
    return mongoose.models.fatmor_record;
  }
}
module.exports = makeModel;