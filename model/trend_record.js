function makeModel (mongoose,...dependencies){
    
  if (!mongoose.models.trend_record){
    const mongoosePaginate = require('mongoose-paginate-v2');
    const idvalidator = require('mongoose-id-validator');
    const myCustomLabels = {
      totalDocs: 'itemCount',
      docs: 'data',
      limit: 'perPage',
      page: 'currentPage',
      nextPage: 'next',
      prevPage: 'prev',
      totalPages: 'pageCount',
      pagingCounter: 'slNo',
      meta: 'paginator',
    };
    mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
    const Schema = mongoose.Schema;
    const schema = new Schema(
      {
        user:{
          type:Schema.Types.ObjectId,
          ref:'user'
        },
        healtstatus:{ type:String },
        oldhealtstatus:{ type:String },
        prediction:{ type:String },
        predictionnumber:{ type:String },
        sehatprediction:{ type:String },
        resikorendahprediction:{ type:String },
        resikotinggiprediction:{ type:String },
        sakitprediction:{ type:String },
        heartrate:{ type:Number },
        heartratestatus:{ type:String },
        bloodoxygen:{ type:Number },
        bloodoxygenstatus:{ type:String },
        temperatur:{ type:String },
        temperaturstatus:{ type:String },
        recomenadtioncode:{ type:String },
        createdAt:{ type:Date },
        updatedAt:{ type:Date },
        isDeleted:{ type:Boolean },
        isActive:{ type:Boolean },
        addedBy:{
          type:Schema.Types.ObjectId,
          ref:'user'
        }
      },
      {
        timestamps: {
          createdAt: 'createdAt',
          updatedAt: 'updatedAt' 
        } 
      }
    );
    
    schema.pre('save', async function (next) {
      this.isDeleted = false;
      this.isActive = true;
      next();
    });

    schema.pre('insertMany', async function (next, docs) {
      if (docs && docs.length){
        for (let index = 0; index < docs.length; index++) {
          const element = docs[index];
          element.isDeleted = false;
          element.isActive = true;
        }
      }
      next();
    });

    schema.method('toJSON', function () {
      const {
        __v, ...object 
      } = this.toObject({ virtuals:true });
      object.id = object._id;
      return object;
    });
    schema.plugin(mongoosePaginate);
    schema.plugin(idvalidator);

    const trend_record = mongoose.model('trend_record',schema,'trend_record');
    return trend_record;
  }
  else {
    return mongoose.models.trend_record;
  }
}
module.exports = makeModel;