function makeModel (mongoose,...dependencies){
    
  if (!mongoose.models.wish_record){
    const mongoosePaginate = require('mongoose-paginate-v2');
    const idvalidator = require('mongoose-id-validator');
    const myCustomLabels = {
      totalDocs: 'itemCount',
      docs: 'data',
      limit: 'perPage',
      page: 'currentPage',
      nextPage: 'next',
      prevPage: 'prev',
      totalPages: 'pageCount',
      pagingCounter: 'slNo',
      meta: 'paginator',
    };
    mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
    const Schema = mongoose.Schema;
    const schema = new Schema(
      {
        user:{
          type:Schema.Types.ObjectId,
          ref:'user'
        },
        temperatur:{
          type:String,
          required:true
        },
        bloodoxygen:{
          type:String,
          required:true
        },
        heartrate:{
          type:String,
          required:true
        },
        createdAt:{
          type:Date,
          required:true
        },
        updatedBy:{
          type:Date,
          required:true
        },
        isDeleted:{ type:Boolean },
        isActive:{ type:Boolean },
        addedBy:{
          type:Schema.Types.ObjectId,
          ref:'user'
        }
      },
      {
        timestamps: {
          createdAt: 'createdAt',
          updatedAt: 'updatedAt' 
        } 
      }
    );
    
    schema.pre('save', async function (next) {
      this.isDeleted = false;
      this.isActive = true;
      next();
    });

    schema.pre('insertMany', async function (next, docs) {
      if (docs && docs.length){
        for (let index = 0; index < docs.length; index++) {
          const element = docs[index];
          element.isDeleted = false;
          element.isActive = true;
        }
      }
      next();
    });

    schema.method('toJSON', function () {
      const {
        __v, ...object 
      } = this.toObject({ virtuals:true });
      object.id = object._id;
      return object;
    });
    schema.plugin(mongoosePaginate);
    schema.plugin(idvalidator);

    const wish_record = mongoose.model('wish_record',schema,'wish_record');
    return wish_record;
  }
  else {
    return mongoose.models.wish_record;
  }
}
module.exports = makeModel;