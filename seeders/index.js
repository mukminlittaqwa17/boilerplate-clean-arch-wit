const db = require('mongoose');
const User = require('../model/user')(db);
const authConstant = require('../constants/authConstant');
const Role = require('../model/role')(db);
const ProjectRoute = require('../model/projectRoute')(db);
const RouteRole = require('../model/routeRole')(db);
const UserRole = require('../model/userRole')(db);
const { replaceAll } = require('../utils/common');

async function seedRole () {
  try {
    const roles = [ 'User', 'System_User' ];
    for (let i = 0; i < roles.length; i++) {
      let result = await Role.findOne({
        name: roles[i],
        code: roles[i].toUpperCase() 
      });
      if (!result) {
        await Role.create({
          name: roles[i],
          code: roles[i].toUpperCase(),
          weight: 1
        });
      }
    };
    console.info('Role model seeded 🍺');
  } catch (error){
    console.log('Role seeder failed.');
  }
}
async function seedProjectRoutes (routes) {
  try {
    if (routes && routes.length) {
      for (let i = 0; i < routes.length; i++) {
        const routeMethods = routes[i].methods;
        for (let j = 0; j < routeMethods.length; j++) {
          const routeObj = {
            uri: routes[i].path.toLowerCase(),
            method: routeMethods[j],
            route_name: `${replaceAll((routes[i].path).toLowerCase().substring(1), '/', '_')}`,
          };
          if (routeObj.route_name){
            let result = await ProjectRoute.findOne(routeObj);
            if (!result) {
              await ProjectRoute.create(routeObj);
            }
          }
        }
      }
      console.info('ProjectRoute model seeded 🍺');
    }
  } catch (error){
    console.log('ProjectRoute seeder failed.');
  }
}
async function seedRouteRole () {
  try {
    const routeRoles = [ 
      {
        route: '/admin/user/create',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/admin/user/create',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/user/addbulk',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/admin/user/addbulk',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/user/list',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/admin/user/list',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/user/:id',
        role: 'User',
        method: 'GET' 
      },
      {
        route: '/admin/user/:id',
        role: 'System_User',
        method: 'GET' 
      },
      {
        route: '/admin/user/:id',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/admin/user/:id',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/user/count',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/admin/user/count',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/user/aggregate',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/admin/user/aggregate',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/user/update/:id',
        role: 'User',
        method: 'PUT' 
      },
      {
        route: '/admin/user/update/:id',
        role: 'System_User',
        method: 'PUT' 
      },
      {
        route: '/admin/user/partial-update/:id',
        role: 'User',
        method: 'PUT'
      },
      {
        route: '/admin/user/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/user/updatebulk',
        role: 'User',
        method: 'PUT' 
      },
      {
        route: '/admin/user/updatebulk',
        role: 'System_User',
        method: 'PUT' 
      },
      {
        route: '/admin/user/softdelete/:id',
        role: 'User',
        method: 'PUT' 
      },
      {
        route: '/admin/user/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/user/softdeletemany',
        role: 'User',
        method: 'PUT' 
      },
      {
        route: '/admin/user/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/user/delete/:id',
        role: 'User',
        method: 'DELETE' 
      },
      {
        route: '/admin/user/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/user/deletemany',
        role: 'User',
        method: 'DELETE' 
      },
      {
        route: '/admin/user/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/sourchrecomendation/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/sourchrecomendation/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/sourchrecomendation/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/sourchrecomendation/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/admin/sourchrecomendation/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/sourchrecomendation/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/sourchrecomendation/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/sourchrecomendation/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/sourchrecomendation/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/sourchrecomendation/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/sourchrecomendation/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/sourchrecomendation/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/sourchrecomendation/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/sourchrecomendation/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/recomendation/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/recomendation/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/recomendation/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/recomendation/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/admin/recomendation/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/recomendation/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/recomendation/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/recomendation/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/recomendation/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/recomendation/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/recomendation/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/recomendation/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/recomendation/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/recomendation/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/trend_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/trend_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/trend_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/trend_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/admin/trend_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/trend_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/trend_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/trend_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/trend_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/trend_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/trend_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/trend_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/trend_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/trend_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/fatmor_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/fatmor_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/fatmor_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/fatmor_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/admin/fatmor_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/fatmor_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/fatmor_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/fatmor_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/fatmor_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/fatmor_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/fatmor_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/fatmor_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/fatmor_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/fatmor_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/wish_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/wish_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/wish_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/wish_record/:id',
        role: 'System_User',
        method: 'GET' 
      },
      {
        route: '/admin/wish_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/wish_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/wish_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/wish_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/wish_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/wish_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/wish_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/wish_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/wish_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/wish_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/kios_sehat_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/kios_sehat_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/kios_sehat_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/kios_sehat_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/admin/kios_sehat_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/kios_sehat_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/kios_sehat_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/kios_sehat_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/kios_sehat_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/kios_sehat_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/kios_sehat_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/kios_sehat_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/kios_sehat_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/kios_sehat_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/usertokens/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/usertokens/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/usertokens/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/usertokens/:id',
        role: 'System_User',
        method: 'GET' 
      },
      {
        route: '/admin/usertokens/:id',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/usertokens/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/usertokens/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/usertokens/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/usertokens/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/usertokens/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/usertokens/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/usertokens/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/usertokens/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/usertokens/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/role/create',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/role/addbulk',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/role/list',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/role/:id',
        role: 'System_User',
        method: 'GET' 
      },
      {
        route: '/admin/role/:id',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/role/count',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/role/aggregate',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/role/update/:id',
        role: 'System_User',
        method: 'PUT' 
      },
      {
        route: '/admin/role/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/role/updatebulk',
        role: 'System_User',
        method: 'PUT' 
      },
      {
        route: '/admin/role/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/role/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/role/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/role/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/projectroute/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/projectroute/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/projectroute/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/projectroute/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/admin/projectroute/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/projectroute/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/projectroute/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/projectroute/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/projectroute/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/projectroute/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/projectroute/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/projectroute/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/projectroute/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/projectroute/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/routerole/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/routerole/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/routerole/list',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/routerole/:id',
        role: 'System_User',
        method: 'GET' 
      },
      {
        route: '/admin/routerole/:id',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/routerole/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/routerole/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/routerole/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/routerole/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/routerole/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/routerole/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/routerole/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/routerole/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/routerole/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/userrole/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/userrole/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/userrole/list',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/userrole/:id',
        role: 'System_User',
        method: 'GET' 
      },
      {
        route: '/admin/userrole/:id',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/userrole/count',
        role: 'System_User',
        method: 'POST' 
      },
      {
        route: '/admin/userrole/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/admin/userrole/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/userrole/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/userrole/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/userrole/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/userrole/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/admin/userrole/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/admin/userrole/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/user/create',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/device/api/v1/user/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/user/addbulk',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/device/api/v1/user/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/user/list',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/device/api/v1/user/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/user/:id',
        role: 'User',
        method: 'GET' 
      },
      {
        route: '/device/api/v1/user/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/user/:id',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/device/api/v1/user/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/user/count',
        role: 'User',
        method: 'POST' 
      },
      {
        route: '/device/api/v1/user/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/user/aggregate',
        role: 'User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/user/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/user/update/:id',
        role: 'User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/partial-update/:id',
        role: 'User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/updatebulk',
        role: 'User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/softdelete/:id',
        role: 'User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/softdeletemany',
        role: 'User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/user/delete/:id',
        role: 'User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/user/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/user/deletemany',
        role: 'User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/user/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/sourchrecomendation/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/sourchrecomendation/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/sourchrecomendation/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/sourchrecomendation/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/sourchrecomendation/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/sourchrecomendation/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/sourchrecomendation/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/sourchrecomendation/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/sourchrecomendation/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/sourchrecomendation/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/sourchrecomendation/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/sourchrecomendation/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/sourchrecomendation/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/sourchrecomendation/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/recomendation/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/recomendation/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/recomendation/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/recomendation/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/recomendation/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/recomendation/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/recomendation/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/recomendation/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/recomendation/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/recomendation/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/recomendation/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/recomendation/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/recomendation/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/recomendation/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/trend_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/trend_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/trend_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/trend_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/trend_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/trend_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/trend_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/trend_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/trend_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/trend_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/trend_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/trend_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/trend_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/trend_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/fatmor_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/fatmor_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/fatmor_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/fatmor_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/fatmor_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/fatmor_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/fatmor_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/fatmor_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/fatmor_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/fatmor_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/fatmor_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/fatmor_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/fatmor_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/fatmor_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/wish_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/wish_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/wish_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/wish_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/wish_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/wish_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/wish_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/wish_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/wish_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/wish_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/wish_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/wish_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/wish_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/wish_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/kios_sehat_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/kios_sehat_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/kios_sehat_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/kios_sehat_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/kios_sehat_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/kios_sehat_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/kios_sehat_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/kios_sehat_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/kios_sehat_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/kios_sehat_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/kios_sehat_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/kios_sehat_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/kios_sehat_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/kios_sehat_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/usertokens/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/usertokens/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/usertokens/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/usertokens/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/usertokens/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/usertokens/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/usertokens/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/usertokens/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/usertokens/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/usertokens/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/usertokens/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/usertokens/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/usertokens/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/usertokens/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/role/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/role/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/role/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/role/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/role/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/role/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/role/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/role/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/role/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/role/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/role/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/role/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/role/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/role/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/projectroute/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/projectroute/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/projectroute/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/projectroute/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/projectroute/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/projectroute/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/projectroute/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/projectroute/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/projectroute/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/projectroute/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/projectroute/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/projectroute/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/projectroute/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/projectroute/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/routerole/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/routerole/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/routerole/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/routerole/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/routerole/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/routerole/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/routerole/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/routerole/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/routerole/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/routerole/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/routerole/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/routerole/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/routerole/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/routerole/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/userrole/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/userrole/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/userrole/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/userrole/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/device/api/v1/userrole/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/userrole/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/userrole/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/device/api/v1/userrole/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/userrole/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/userrole/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/userrole/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/userrole/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/device/api/v1/userrole/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/device/api/v1/userrole/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/sourchrecomendation/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/sourchrecomendation/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/sourchrecomendation/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/sourchrecomendation/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/sourchrecomendation/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/sourchrecomendation/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/sourchrecomendation/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/sourchrecomendation/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/sourchrecomendation/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/sourchrecomendation/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/sourchrecomendation/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/sourchrecomendation/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/sourchrecomendation/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/sourchrecomendation/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/recomendation/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/recomendation/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/recomendation/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/recomendation/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/recomendation/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/recomendation/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/recomendation/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/recomendation/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/recomendation/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/recomendation/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/recomendation/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/recomendation/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/recomendation/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/recomendation/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/trend_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/trend_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/trend_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/trend_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/trend_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/trend_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/trend_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/trend_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/trend_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/trend_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/trend_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/trend_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/trend_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/trend_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/fatmor_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/fatmor_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/fatmor_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/fatmor_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/fatmor_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/fatmor_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/fatmor_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/fatmor_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/fatmor_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/fatmor_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/fatmor_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/fatmor_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/fatmor_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/fatmor_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/wish_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/wish_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/wish_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/wish_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/wish_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/wish_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/wish_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/wish_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/wish_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/wish_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/wish_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/wish_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/wish_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/wish_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/kios_sehat_record/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/kios_sehat_record/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/kios_sehat_record/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/kios_sehat_record/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/kios_sehat_record/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/kios_sehat_record/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/kios_sehat_record/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/kios_sehat_record/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/kios_sehat_record/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/kios_sehat_record/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/kios_sehat_record/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/kios_sehat_record/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/kios_sehat_record/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/kios_sehat_record/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/user/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/user/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/user/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/user/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/user/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/user/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/user/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/user/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/user/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/user/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/user/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/user/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/user/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/user/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/usertokens/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/usertokens/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/usertokens/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/usertokens/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/usertokens/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/usertokens/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/usertokens/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/usertokens/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/usertokens/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/usertokens/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/usertokens/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/usertokens/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/usertokens/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/usertokens/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/role/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/role/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/role/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/role/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/role/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/role/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/role/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/role/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/role/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/role/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/role/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/role/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/role/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/role/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/projectroute/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/projectroute/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/projectroute/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/projectroute/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/projectroute/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/projectroute/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/projectroute/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/projectroute/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/projectroute/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/projectroute/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/projectroute/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/projectroute/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/projectroute/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/projectroute/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/routerole/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/routerole/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/routerole/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/routerole/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/routerole/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/routerole/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/routerole/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/routerole/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/routerole/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/routerole/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/routerole/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/routerole/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/routerole/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/routerole/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/userrole/create',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/userrole/addbulk',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/userrole/list',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/userrole/:id',
        role: 'System_User',
        method: 'GET'
      },
      {
        route: '/client/api/v1/userrole/:id',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/userrole/count',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/userrole/aggregate',
        role: 'System_User',
        method: 'POST'
      },
      {
        route: '/client/api/v1/userrole/update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/userrole/partial-update/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/userrole/updatebulk',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/userrole/softdelete/:id',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/userrole/softdeletemany',
        role: 'System_User',
        method: 'PUT'
      },
      {
        route: '/client/api/v1/userrole/delete/:id',
        role: 'System_User',
        method: 'DELETE'
      },
      {
        route: '/client/api/v1/userrole/deletemany',
        role: 'System_User',
        method: 'DELETE'
      },

    ];
    if (routeRoles && routeRoles.length) {
      for (let i = 0; i < routeRoles.length; i++) {
        let route = await ProjectRoute.findOne({
          uri: routeRoles[i].route.toLowerCase(),
          method: routeRoles[i].method,
          isActive: true,
          isDeleted: false 
        }, { id: 1 });
        let role = await Role.findOne({
          code: (routeRoles[i].role).toUpperCase(),
          isActive: true,
          isDeleted: false 
        }, { id: 1 });
        if (route && route.id && role && role.id) {
          let routeRoleObj = await RouteRole.findOne({
            roleId: role.id,
            routeId: route.id
          });
          if (!routeRoleObj) {
            await RouteRole.create({
              roleId: role.id,
              routeId: route.id
            });
          }
        }
      };
      console.info('RouteRole model seeded 🍺');
    }
  } catch (error){
    console.log('RouteRole seeder failed.');
  }
}
async function seedUserRole (){
  try {
    let user = await User.findOne({
      'username':'test_user3',
      'isActive':true,
      'isDeleted':false
    });
    let userRole = await Role.findOne({ code: 'SYSTEM_USER' }, { id: 1 });
    if (user && user.isPasswordMatch('qwerty21') && userRole){
      let count = await UserRole.countDocuments({
        userId: user.id,
        roleId: userRole.id
      });
      if (count == 0) {
        await UserRole.create({
          userId: user.id,
          roleId: userRole.id 
        });
        console.info('user seeded 🍺');
      }   
    }
    let admin = await User.findOne({
      'username':'test_user1',
      'isActive':true,
      'isDeleted':false
    });
    let adminRole = await Role.findOne({ code: 'SYSTEM_USER' }, { id: 1 });
    if (admin && admin.isPasswordMatch('qwerty21') && adminRole){
      let count = await UserRole.countDocuments({
        userId: admin.id,
        roleId: adminRole.id
      });
      if (count == 0) {
        await UserRole.create({
          userId: admin.id,
          roleId: adminRole.id 
        });
        console.info('admin seeded 🍺');
      }   
    }
  } catch (error){
    console.log('UserRole seeder failed.');
  }
}
async function seedUser () {
  try {
    let user = await User.findOne({
      'username':'test_user1',
      'isActive':true,
      'isDeleted':false
    });
    if (!user || !user.isPasswordMatch('qwerty21') ) {
      let user = new User({
        'password':'qwerty21',
        'username':'test_user1',
        'fullname':'test_user1',
        'email':'Wit_test@gmail.com',
        'phonenumber':'077172312313',
        'role':authConstant.USER_ROLE.User
      });
      await User.create(user);
    }
    let admin = await User.findOne({
      'username':'user_test_2',
      'isActive':true,
      'isDeleted':false
    });
    if (!admin || !admin.isPasswordMatch('qwerty21') ) {
      let admin = new User({
        'password':'qwerty21',
        'username':'test_user1',
        'fullname':'test_user1',
        'email':'Wit_test@gmail.com',
        'phonenumber':'077172312313',
        'role':authConstant.USER_ROLE.Admin
      });
      await User.create(admin);
    }
    console.info('Users seeded🍺');
  } catch (error){
    console.log('Users seeder failed.');
  }
}

async function seedData (allRegisterRoutes){
  await seedUser();
  await seedRole();
  await seedProjectRoutes(allRegisterRoutes);
  await seedRouteRole();
  await seedUserRole();
}     

module.exports = seedData;